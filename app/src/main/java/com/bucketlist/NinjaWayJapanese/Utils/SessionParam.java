package com.bucketlist.NinjaWayJapanese.Utils;

import android.content.Context;
import android.content.SharedPreferences;


public class SessionParam {
    String PREF_NAME = "MyPref";

    Context _context;
    int PRIVATE_MODE = 0;
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_ID = "id";
    public static final String KEY_FULLNAME = "full_name";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_ACCESS_TOKEN = "access_token";


    public void SessionParam() {
    }


    public String answer1 = "", answer2 = "", answer3 = "", answer4 = "", answer5 = "", deviceToken = "";
    String stripToken = "";
    public String email = "";
    public String paymentStatus = "";


    public SessionParam(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        this.answer1 = sharedPreferences.getString("answer1", "");
        this.answer2 = sharedPreferences.getString("answer2", "");
        this.answer3 = sharedPreferences.getString("answer3", "");
        this.answer4 = sharedPreferences.getString("answer4", "");
        this.answer5 = sharedPreferences.getString("answer5", "");
        this.deviceToken = sharedPreferences.getString("devicetoken", "");
        this.email = sharedPreferences.getString("email", "");
        this.paymentStatus = sharedPreferences.getString("paymentStatus", "");
//      this.paymentStatus = "y";
    }

    public void persist(Context context, int no) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        if (no == 1) {
            prefsEditor.putString("answer1", "y");
        }
        if (no == 2) {
            prefsEditor.putString("answer2", "y");
        }
        if (no == 3) {
            prefsEditor.putString("answer3", "y");
        }
        if (no == 4) {
            prefsEditor.putString("answer4", "y");
        }
        if (no == 5) {
            prefsEditor.putString("answer5", "y");
        }
        prefsEditor.commit();
    }

    public void redOne(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("answer1", "");
        prefsEditor.commit();
    }

    public void blueOne(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("answer2", "");
        prefsEditor.commit();
    }

    public void greenOne(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("answer3", "");
        prefsEditor.commit();
    }

    public void purpleOne(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("answer4", "");
        prefsEditor.commit();
    }

    public void clearPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        // AccessToken.setCurrentAccessToken(null);
        //  LoginManager.getInstance().logOut();
        prefsEditor.clear();
        prefsEditor.commit();
    }

    public void saveToken(Context context, String Token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("devicetoken", Token);
        prefsEditor.commit();
    }

    public void saveEmail(Context context, String email) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("email", email);
        prefsEditor.commit();
    }

    public void paymentStatus(Context context, String paymentStatus) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("paymentStatus", paymentStatus);
        prefsEditor.commit();
    }

    @Override
    public String toString() {
        return "SessionParam [name=" + "]";
    }


}
