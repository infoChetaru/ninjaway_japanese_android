package com.bucketlist.NinjaWayJapanese.Utils;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Config;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;


/**
 * Base activity class. This may be useful in
 * Implementing google analytics or
 * Any app wise implementation
 */
public abstract class BaseActivity extends AppCompatActivity {
    private static List<Activity> sActivities = new ArrayList<Activity>();
    public Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=this;
        sActivities.add(this);
    }


    /*public void setAppBar(String title, boolean isBackVisible) {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isBackVisible);
        getSupportActionBar().setDisplayShowHomeEnabled(isBackVisible);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                hideSoftKeyboard();
            }
        });
        getSupportActionBar().setTitle(title);

        //   existingMsg

    }*/

    @Override
    protected void onResume() {
        super.onResume();
        //   hideSoftKeyboard();
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        //  hideSoftKeyboard();
//    }


    public void hideSoftKeyboard() {
     //   getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) this
                    .getSystemService(Activity
                            .INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken
                    (), 0);
        } catch (Exception e) {

        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        sActivities.remove(this);
        super.onDestroy();
    }

    public void finishAllActivities() {
        if (sActivities != null) {
            for (Activity activity : sActivities) {
                if (Config.DEBUG) {
                    Log.d("BaseActivity", "finishAllActivities activity=" + activity.getClass()
                            .getSimpleName());
                }
                activity.finish();
            }
        }
    }

    public static void finishAllActivitiesStatic() {
        if (sActivities != null) {
            for (Activity activity : sActivities) {
                if (Config.DEBUG) {
                    Log.d("BaseActivity", "finishAllActivities activity=" + activity.getClass()
                            .getSimpleName());
                }
                activity.finish();
            }
        }
    }

    public String getAppString(int id) {
        String str = "";
        if (!TextUtils.isEmpty(this.getResources().getString(id))) {
            str = this.getResources().getString(id);
        } else {
            str = "";
        }
        return str;
    }

}
