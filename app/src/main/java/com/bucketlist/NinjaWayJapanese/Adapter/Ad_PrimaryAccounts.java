package com.bucketlist.NinjaWayJapanese.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bucketlist.NinjaWayJapanese.R;
import com.bucketlist.NinjaWayJapanese.Utils.Utility;

import java.util.List;

public class Ad_PrimaryAccounts extends RecyclerView.Adapter<Ad_PrimaryAccounts.ViewHolder> {
    private Context context;
    List<String> list;
    Utility utility=new Utility();
    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_name;
        public  ViewHolder(View view){
            super(view);
            tv_name=view.findViewById(R.id.tv_name);
        }
    }
    public Ad_PrimaryAccounts(List<String> list, Context mContext){
        this.list=list;
        notifyDataSetChanged();
        this.context=mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return  new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_primaryaccount,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.tv_name.setText(list.get(i).toLowerCase());
    }

    public  int getItemCount(){
        return this.list.size();
    }
}
