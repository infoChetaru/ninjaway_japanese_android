package com.bucketlist.NinjaWayJapanese.retrofit;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;


import androidx.appcompat.app.AlertDialog;

import com.bucketlist.NinjaWayJapanese.R;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by prakhar on 11/17/2016.
 */
public class BaseRequest<T> extends BaseRequestParser {
    private Context mContext;
    private ApiInterface apiInterface;
    private RequestReciever requestReciever;
    private boolean runInBackground = false;
    private Dialog dialog;
    private View loaderView = null;
    private int APINumber_ = 1;
    private boolean showErrorDialog = true;

    public boolean isRunInBackground() {
        return runInBackground;
    }

    public void setRunInBackground(boolean runInBackground) {
        this.runInBackground = runInBackground;
    }

    public void setLoaderView(View loaderView_) {
        this.loaderView = loaderView_;
    }

    public BaseRequest(Context context) {
        mContext = context;
        apiInterface =
                ApiClient.getClient().create(ApiInterface.class);
        dialog = getProgressesDialog(context);
        //dialog.setTitle("Fetching details...");

    }

    public BaseRequest(Context context, Fragment fm) {
        mContext = context;
        apiInterface =
                ApiClient.getClient().create(ApiInterface.class);
        dialog = getProgressesDialog(context);
    }

    public void setBaseRequestListner(RequestReciever requestListner) {
        this.requestReciever = requestListner;

    }

    public ArrayList<Object> getDataList(JSONArray mainArray, Class<T> t) {
        ArrayList<Object> list = null;
        list = new ArrayList<>();
        try {
            Gson gsm = null;
            if (null != mainArray) {
                for (int i = 0; i < mainArray.length(); i++) {
                    gsm = new Gson();
                    Object object = gsm.fromJson(mainArray.optJSONObject(i).toString(), t);
                    list.add(object);
                }
            }
            return list;
        } catch (NullPointerException npe) {
            return list;
        }
    }

    public Callback<JsonElement> responseCallback = new Callback<JsonElement>() {
        @Override
        public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
            try {
                String responseServer = "";
                hideLoader();
                if (null != response.body()) {
                    JsonElement jsonElement = (JsonElement) response.body();
                    if (null != jsonElement) {
                        responseServer = jsonElement.toString();
                    }

                } else if (response.errorBody() != null) {
                    responseServer = readStreamFully(response.errorBody().contentLength(),
                            response.errorBody().byteStream());
                }
                logFullResponse(responseServer, "OUTPUT");
                try {
                    if (parseJson(responseServer)) {
                        if (null != requestReciever) {
                            if (null != getDataArray()) {
                                requestReciever.onSuccess(APINumber_, responseServer, getDataArray());
                            } else if (null != getDataObject()) {
                                requestReciever.onSuccess(APINumber_, responseServer, getDataObject());
                            } else {
                                requestReciever.onSuccess(APINumber_, responseServer, message);
                            }
                        }
                    } else {
                        if (null != requestReciever) {
                            requestReciever.onFailure(1, "" + mResponseCode, message);
                        }
                    }
                } catch (NullPointerException npe) {

                }
            } catch (NullPointerException npe) {

            }
        }

        @Override
        public void onFailure(Call<JsonElement> call, Throwable t) {

            handler.removeCallbacksAndMessages(null);
            handler.postDelayed(r, 1000);
           /* if (t.getMessage().startsWith("Unable to resolve")) {
               r.run();
            }*/


        }


    };


    public Callback<JsonElement> responseCallbackCustom = new Callback<JsonElement>() {
        @Override
        public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
            try {
                String responseServer = "";
                hideLoader();
                if (null != response.body()) {
                    JsonElement jsonElement = (JsonElement) response.body();
                    if (null != jsonElement) {
                        responseServer = jsonElement.toString();
                    }

                } else if (response.errorBody() != null) {
                    responseServer = readStreamFully(response.errorBody().contentLength(),
                            response.errorBody().byteStream());
                }
                logFullResponse(responseServer, "OUTPUT");
                requestReciever.onSuccess(APINumber_, responseServer, null);
            } catch (NullPointerException npe) {
            }


        }

        @Override
        public void onFailure(Call<JsonElement> call, Throwable t) {

            handler.removeCallbacksAndMessages(null);
            handler.postDelayed(r, 1000);
           /* if (t.getMessage().startsWith("Unable to resolve")) {
               r.run();
            }*/


        }
    };
    private RequestType requestType = null;

    public enum RequestType {
        Post, Get
    };

    String network_error_message = "Check internet connection";
    Handler handler = new Handler();
    Runnable r = new Runnable() {
        @Override
        public void run() {
            hideLoader();
            try{
                if (null != requestReciever) {
                    requestReciever.onNetworkFailure(APINumber_, network_error_message);
                }
                if (showErrorDialog) {
                }
            }catch (IllegalStateException ise){

            }
        }
    };

    public void callAPIPost(final int APINumber, JsonObject jsonObject, String remainingURL) {
        try {
            requestType = RequestType.Post;
            APINumber_ = APINumber;
            showLoader();
            if (jsonObject == null) {
                jsonObject = new JsonObject();
            }
            String baseURL = ApiClient.getClient().baseUrl().toString() + remainingURL;
            Log.i("BaseReq",
                    "Url" + " : "
                            + baseURL);
            logFullResponse(jsonObject.toString(), "INPUT");
            Call<JsonElement> call = apiInterface.postData(remainingURL, jsonObject);

            call.enqueue(responseCallback);
        } catch (NullPointerException npe) {
        }

    }

    public void callAPIPostwoLoader(final int APINumber, JsonObject jsonObject, String remainingURL) {
        try {
            requestType = RequestType.Post;
            APINumber_ = APINumber;
            //showLoader();

            if (jsonObject == null) {
                jsonObject = new JsonObject();
            }

            String baseURL = ApiClient.getClient().baseUrl().toString() + remainingURL;
            Log.i("BaseReq",
                    "Url" + " : "
                            + baseURL);
            logFullResponse(jsonObject.toString(), "INPUT");
            Call<JsonElement> call = apiInterface.postData(remainingURL, jsonObject);

            call.enqueue(responseCallback);
        } catch (NullPointerException npe) {
        }
    }

    public void callAPIPostIMAGE(final int APINumber, JsonObject jsonObject, String remainingURL, MultipartBody.Part body) {

        APINumber_ = APINumber;
        showLoader();

        if (jsonObject == null) {
            jsonObject = new JsonObject();
        }

        String baseURL = ApiClient.getClient().baseUrl().toString() + remainingURL;
        Log.i("BaseReq",
                "Url" + " : "
                        + baseURL);
        logFullResponse(jsonObject.toString(), "INPUT");
        Call<JsonElement> call = apiInterface.uploadImage(body);

        call.enqueue(responseCallback);
    }

    public void callAPIGET(final int APINumber, Map<String, String> map, String remainingURL) {
        APINumber_ = APINumber;
        requestType = RequestType.Post;
        showLoader();
        String baseURL = ApiClient.getClient().baseUrl().toString() + remainingURL;
        if (!baseURL.endsWith("?")) {
            baseURL = baseURL + "?";
        }

        for (Map.Entry<String, String> entry : map.entrySet()) {
            baseURL = baseURL + entry.getKey() + "=" + entry.getValue() + "&";
        }
        System.out.println("BaseReq INPUT URL : " + baseURL);
        Call<JsonElement> call = apiInterface.postDataGET(remainingURL, map);
        call.enqueue(responseCallback);
    }

    public void callAPIGETCustomURL(final int APINumber, Map<String, String> map, String baseURL_) {
        APINumber_ = APINumber;

        showLoader();
        String baseURL = baseURL_;
        if (!baseURL.endsWith("?")) {
            baseURL = baseURL + "?";
        }

        for (Map.Entry<String, String> entry : map.entrySet()) {
            baseURL = baseURL + entry.getKey() + "=" + entry.getValue() + "&";
        }
        System.out.println("BaseReq INPUT URL : " + baseURL);
        ApiInterface apiInterface_ = ApiClient.getCustomClient(baseURL_).create(ApiInterface.class);


        Call<JsonElement> call = apiInterface_.postDataGET("", map);
        call.enqueue(responseCallbackCustom);
    }

    public void logFullResponse(String response, String inout) {
        final int chunkSize = 3000;
        try {
            if (null != response && response.length() > chunkSize) {
                int chunks = (int) Math.ceil((double) response.length()
                        / (double) chunkSize);


                for (int i = 1; i <= chunks; i++) {
                    if (i != chunks) {
                        Log.i("BaseReq",
                                inout + " : "
                                        + response.substring((i - 1) * chunkSize, i
                                        * chunkSize));
                    } else {
                        Log.i("BaseReq",
                                inout + " : "
                                        + response.substring((i - 1) * chunkSize,
                                        response.length()));
                    }
                }
            } else {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.d("BaseReq", inout + " : " + jsonObject.toString(jsonObject.length()));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("BaseReq", " logFullResponse=>  " + response);
                }

            }
        }catch (NullPointerException npe){}
        }

    private String readStreamFully(long len, InputStream inputStream) {
        try{
        if (inputStream == null) {
            return null;
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
        }catch (NullPointerException npe){
            return "";
        }
    }

    public Dialog getProgressesDialog(Context ct) {

        try {
            Dialog dialog = new Dialog(ct);
             dialog = new Dialog(ct);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //dialog.setTitle("Fetching details...");
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.progress_dialog_loader);
            dialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            return dialog;
        }catch (NullPointerException npe){
            return dialog;
        }
    }

    public void showErrorDialog(Context ct, String msg, final int APINumber, final JsonObject jsonObject, String remainingURL) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ct);
        alertDialog.setMessage(msg);
        alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog.show();

    }

    public void showLoader() {
        if (mContext != null && !((Activity) mContext).isDestroyed()) {

            if (!runInBackground) {
                if (null != loaderView) {
                    loaderView.setVisibility(View.VISIBLE);
                } else if (null != dialog) {

                    dialog.show();

                }
            }
        }
    }

    public void hideLoader() {
        if (mContext != null && !((Activity) mContext).isDestroyed()) {

            if (!runInBackground) {
                if (null != loaderView) {
                    loaderView.setVisibility(View.GONE);
                } else if (null != dialog) {
                    dialog.dismiss();
                }
            }
        }
    }

    public int TYPE_NOT_CONNECTED = 0;
    public int TYPE_WIFI = 1;
    public int TYPE_MOBILE = 2;

    public int getConnectivityStatus(Context context) {
        if (null == context) {
            return TYPE_NOT_CONNECTED;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        if (null != activeNetwork && activeNetwork.isConnected()) {

            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return TYPE_WIFI;
            }

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return TYPE_MOBILE;
            }
        }
        return TYPE_NOT_CONNECTED;


    }
}
