package com.bucketlist.NinjaWayJapanese.retrofit;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;


public abstract class BaseRequestParser {
    public String message = "Something going wrong.";
    boolean mResponseCode;


    private JSONObject mRespJSONObject = null;

    public boolean parseJson(String json) {
        if (!TextUtils.isEmpty(json)) {
            try {
                mRespJSONObject = new JSONObject(json);
                if (null != mRespJSONObject) {

                    mResponseCode = mRespJSONObject.optBoolean("status",
                            false);
                    message = mRespJSONObject.optString("message",
                            "Something going wrong.");
                    if (mResponseCode) {
                        return true;
                    } else {
                        return false;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }


    public JSONArray getDataArray() {
        if (null == mRespJSONObject) {
            return null;
        }

        try {
            if (mRespJSONObject.optJSONArray("result") != null) {
                return mRespJSONObject.optJSONArray("result");

            }else if(mRespJSONObject.optJSONArray("response") != null){
                return mRespJSONObject.optJSONArray("response");
            }

            else {
                return mRespJSONObject.optJSONArray("jobs");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    public Object getDataObject() {
        if (null == mRespJSONObject) {
            return null;
        }


        try {
            if (mRespJSONObject.optJSONObject("result") != null) {
                return mRespJSONObject.optJSONObject("result");
            } else if(mRespJSONObject.optJSONObject("record")!=null){
                return mRespJSONObject.optJSONObject("record");
            }else/* if(mRespJSONObject.optJSONObject("response")!=null)*/{
                return mRespJSONObject.optJSONObject("response");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
