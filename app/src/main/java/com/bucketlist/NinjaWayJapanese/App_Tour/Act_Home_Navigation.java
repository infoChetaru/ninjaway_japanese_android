package com.bucketlist.NinjaWayJapanese.App_Tour;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bucketlist.NinjaWayJapanese.Questions.Act_Asakusa;
import com.bucketlist.NinjaWayJapanese.R;
import com.bucketlist.NinjaWayJapanese.Utils.BaseActivity;
import com.bucketlist.NinjaWayJapanese.Utils.SessionParam;
import com.google.android.material.navigation.NavigationView;


public class Act_Home_Navigation extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    View v_asakusa, iv_dialog;
    SessionParam sessionParam;
    DrawerLayout drawer;
    ImageView iv_contact;
    RelativeLayout rv_main;
    View v_link1, v_link2;
    final static int requestcode = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_navigation_home);


        if (Build.VERSION.SDK_INT >= 21) {
            // getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.dark_nav)); // Navigation bar the soft bottom of some phones like nexus and some Samsung note series
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.navi_orange)); //status bar or the time bar at the top
        }



        drawer = (DrawerLayout) findViewById(R.id.drawer_layoutnew);
        ImageButton menuRight = (ImageButton) findViewById(R.id.menuRight);
        iv_contact = findViewById(R.id.iv_contact);
        rv_main = findViewById(R.id.rv_main);
        v_link1 = findViewById(R.id.v_link1);
        v_link2 = findViewById(R.id.v_link2);
        rv_main.setBackgroundColor(getResources().getColor(R.color.relative_background));
        rv_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iv_contact.setVisibility(View.GONE);
                v_link1.setVisibility(View.GONE);
                v_link2.setVisibility(View.GONE);
                rv_main.setVisibility(View.GONE);
            }
        });

        v_link1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(mContext, "test", Toast.LENGTH_SHORT).show();
                Uri uri = Uri.parse("https://www.bucketlist.co.jp/"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        v_link2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(mContext, "2", Toast.LENGTH_SHORT).show();
//                Intent emailIntent = new Intent(Intent.ACTION_SEND);
//                emailIntent.setType("text/plain");
//                startActivity(emailIntent);
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "ninjaway@bucketlist.co.jp"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    //TODO smth
                }
            }
        });

        menuRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
            }
        });
        NavigationView navigationView2 = (NavigationView) findViewById(R.id.nav_view2);
        navigationView2.setNavigationItemSelectedListener(this);

        //----------------------------------------------------
       /* Menu m = navigationView2.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            //for applying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
*/
        //--------------------------------------------------
        v_asakusa = findViewById(R.id.v_asakusa);
        iv_dialog = findViewById(R.id.iv_dialog);
        sessionParam = new SessionParam(mContext);

        if (sessionParam.deviceToken.equals("")) {
            sessionParam.saveToken(mContext, "yes");
        }
        v_asakusa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, Act_Asakusa.class));
            }
        });
        iv_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(mContext, "open dialog", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layoutnew);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/fonts.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            //Toast.makeText(this, "You have chosen " + "1", Toast.LENGTH_LONG).show();
            drawer.closeDrawer(GravityCompat.END);

        } else if (id == R.id.nav_play) {
            Intent in = new Intent(Act_Home_Navigation.this, WelcomeActivity.class);
            in.putExtra("navi", "wellcome");
            startActivity(in);
            //         Toast.makeText(this, "You have chosen " + "2", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_contact) {
            rv_main.setVisibility(View.VISIBLE);
            iv_contact.setVisibility(View.VISIBLE);
            v_link1.setVisibility(View.VISIBLE);
            v_link2.setVisibility(View.VISIBLE);
            //call_dialogbox();
//          Toast.makeText(this, "You have chosen " + "3", Toast.LENGTH_LONG).show();
        }
//      Toast.makeText(this, "You have chosen " + "all", Toast.LENGTH_LONG).show();
//      DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layoutnew);
//      drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    private void call_dialogbox() {
        /*AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_contact, null);
        dialogBuilder.setView(dialogView);
//      EditText editText = (EditText) dialogView.findViewById(R.id.label_field);
//      editText.setText("test label");
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();*/
        Dialog dialog = new Dialog(this);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setTitle("Fetching details...");
        dialog.setContentView(R.layout.dialog_contact);
        dialog.setCanceledOnTouchOutside(true);

        Window window = dialog.getWindow();
        window.setGravity(Gravity.TOP);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
       /* WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.gravity = Gravity.TOP;
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        window.setAttributes(lp);*/
        dialog.show();
    }




//    public void getEmail(){
//        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
//        Account[] accounts = AccountManager.get(mContext).getAccounts();
//        for (Account account : accounts) {
//            if (emailPattern.matcher(account.name).matches()) {
//                String possibleEmail = account.name;
//
//                Log.d("Email",possibleEmail);
//
//            }
//        }
//    }

}
