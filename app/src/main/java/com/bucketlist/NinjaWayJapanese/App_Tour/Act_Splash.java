package com.bucketlist.NinjaWayJapanese.App_Tour;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.bucketlist.NinjaWayJapanese.R;
import com.bucketlist.NinjaWayJapanese.Utils.BaseActivity;


public class Act_Splash extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_splash);

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                startActivity(new Intent(Act_Splash.this, WelcomeActivity.class).putExtra("navi",""));

                finish();
            }
        }, 3000);
    }
}
