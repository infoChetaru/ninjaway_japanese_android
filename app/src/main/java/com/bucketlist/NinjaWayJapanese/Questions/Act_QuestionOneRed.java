package com.bucketlist.NinjaWayJapanese.Questions;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.InflateException;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import androidx.core.content.ContextCompat;

import com.bucketlist.NinjaWayJapanese.App_Tour.Act_Home_Navigation;
import com.bucketlist.NinjaWayJapanese.Map.Act_ImageMap;
import com.bucketlist.NinjaWayJapanese.ModelAnswers;
import com.bucketlist.NinjaWayJapanese.R;
import com.bucketlist.NinjaWayJapanese.Utils.BaseActivity;
import com.bucketlist.NinjaWayJapanese.Utils.SessionParam;
import com.bucketlist.NinjaWayJapanese.Utils.Utility;
import com.bucketlist.NinjaWayJapanese.retrofit.BaseRequest;
import com.bucketlist.NinjaWayJapanese.retrofit.Functions;
import com.bucketlist.NinjaWayJapanese.retrofit.RequestReciever;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class Act_QuestionOneRed extends BaseActivity implements View.OnClickListener {
    TextView tv_welcome, tv_line2, tv_line3, tv_line4, tv_line5, tv_line6, tv_line7, tv_answer;
    ImageView cloud, iv_back, iv_top;
    ScrollView sv_main;

    ImageView cross1, cross2, cross3, cross4, cross5, cross6, cross7, cross8, cross9, cross10, cross11, cross12,
            cross13, cross14, cross15, cross16, cross17, cross18, cross19, cross20, cross21, cross22, cross23, cross24, cross25;
    LinearLayout ll_answerblock,red_answer_layout;
    ArrayList<ModelAnswers> list = new ArrayList<>();
    SessionParam sessionParam;
    private BaseRequest baseRequest;
    Utility utility;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_questiond);
        sessionParam = new SessionParam(mContext);
        if (Build.VERSION.SDK_INT >= 21) {
            // getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.dark_nav)); // Navigation bar the soft bottom of some phones like nexus and some Samsung note series
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.status_bar_red)); //status bar or the time bar at the top
        }

        for (int i = 0; i < 25; i++) {
            ModelAnswers modelAnswers = new ModelAnswers();
            modelAnswers.setCheck(false);
            list.add(modelAnswers);
        }
        utility = new Utility();

        tv_welcome = findViewById(R.id.tv_welcome);
        tv_line2 = findViewById(R.id.tv_line2);
        tv_line3 = findViewById(R.id.tv_line3);
        tv_line4 = findViewById(R.id.tv_line4);
        tv_line4.setText(R.string.red_ninja_line_four);
        tv_line5 = findViewById(R.id.tv_line5);
        tv_line6 = findViewById(R.id.tv_line6);
        tv_line7 = findViewById(R.id.tv_line7);
        tv_answer = findViewById(R.id.tv_answer);
        red_answer_layout = findViewById(R.id.red_answer_layout);
        ll_answerblock = findViewById(R.id.ll_answerblock);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/fonts.otf");
        tv_welcome.setTypeface(face);
        tv_line2.setTypeface(face);
        tv_line3.setTypeface(face);
        tv_line4.setTypeface(face);
        tv_line5.setTypeface(face);
        tv_line6.setTypeface(face);
        tv_line7.setTypeface(face);
        tv_answer.setTypeface(face);
        cloud = findViewById(R.id.cloud);
        cross1 = findViewById(R.id.cross1);
        cross2 = findViewById(R.id.cross2);
        cross3 = findViewById(R.id.cross3);
        cross4 = findViewById(R.id.cross4);
        cross5 = findViewById(R.id.cross5);
        cross6 = findViewById(R.id.cross6);
        cross7 = findViewById(R.id.cross7);
        cross8 = findViewById(R.id.cross8);
        cross9 = findViewById(R.id.cross9);
        cross10 = findViewById(R.id.cross10);
        cross11 = findViewById(R.id.cross11);
        cross12 = findViewById(R.id.cross12);
        cross13 = findViewById(R.id.cross13);
        cross14 = findViewById(R.id.cross14);
        cross15 = findViewById(R.id.cross15);
        cross16 = findViewById(R.id.cross16);
        cross17 = findViewById(R.id.cross17);
        cross18 = findViewById(R.id.cross18);
        cross19 = findViewById(R.id.cross19);
        cross20 = findViewById(R.id.cross20);
        cross21 = findViewById(R.id.cross21);
        cross22 = findViewById(R.id.cross22);
        cross23 = findViewById(R.id.cross23);
        cross24 = findViewById(R.id.cross24);
        cross25 = findViewById(R.id.cross25);

        iv_back = findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(mContext,Act_ImageMap.class));
            }
        });





        /*iv_bottom = findViewById(R.id.iv_bottom);
        sv_main = findViewById(R.id.sv_main);
        iv_top = findViewById(R.id.iv_top);
        iv_bottom.setImageResource(R.drawable.back_red_bottom);
        sv_main.setBackgroundResource(R.drawable.back_red);
        iv_top.setBackgroundResource(R.drawable.back_red_bottom);*/

        if (sessionParam.answer1.equals("y")) {
            cross1.setImageResource(R.drawable.cross);
            cross2.setImageResource(R.drawable.cross);
            cross3.setImageResource(R.drawable.cross);
            cross8.setImageResource(R.drawable.cross);
            cloud.setVisibility(View.GONE);

            //test
            tv_answer.setVisibility(View.GONE);
            red_answer_layout.setVisibility(View.GONE);
            //tv_answer.setOnClickListener(this);

        } else {
            cross1.setOnClickListener(this);
            cross2.setOnClickListener(this);
            cross3.setOnClickListener(this);
            cross4.setOnClickListener(this);
            cross5.setOnClickListener(this);
            cross6.setOnClickListener(this);
            cross7.setOnClickListener(this);
            cross8.setOnClickListener(this);
            cross9.setOnClickListener(this);
            cross10.setOnClickListener(this);
            cross11.setOnClickListener(this);
            cross12.setOnClickListener(this);
            cross13.setOnClickListener(this);
            cross14.setOnClickListener(this);
            cross15.setOnClickListener(this);
            cross16.setOnClickListener(this);
            cross17.setOnClickListener(this);
            cross18.setOnClickListener(this);
            cross19.setOnClickListener(this);
            cross20.setOnClickListener(this);
            cross21.setOnClickListener(this);
            cross22.setOnClickListener(this);
            cross23.setOnClickListener(this);
            cross24.setOnClickListener(this);
            cross25.setOnClickListener(this);
            tv_answer.setOnClickListener(this);
            red_answer_layout.setOnClickListener(this);
        }

        if (sessionParam.answer2.equals("y")) {
            cross16.setImageResource(R.drawable.cross_gray);
            cross21.setImageResource(R.drawable.cross_gray);
            cloud.setVisibility(View.GONE);
            cross16.setClickable(false);
            cross21.setClickable(false);
        }
        if (sessionParam.answer3.equals("y")) {
            cross5.setImageResource(R.drawable.cross_gray);
            cross10.setImageResource(R.drawable.cross_gray);
            cross11.setImageResource(R.drawable.cross_gray);
            cross12.setImageResource(R.drawable.cross_gray);
            cross13.setImageResource(R.drawable.cross_gray);
            cross14.setImageResource(R.drawable.cross_gray);
            cross15.setImageResource(R.drawable.cross_gray);
            cross5.setClickable(false);
            cross10.setClickable(false);
            cross11.setClickable(false);
            cross12.setClickable(false);
            cross13.setClickable(false);
            cross14.setClickable(false);
            cross15.setClickable(false);
            //cloud.setVisibility(l.GONE);
        }
        if (sessionParam.answer4.equals("y")) {
            cross18.setImageResource(R.drawable.cross_gray);
            cross23.setImageResource(R.drawable.cross_gray);
            cross24.setImageResource(R.drawable.cross_gray);
            cross25.setImageResource(R.drawable.cross_gray);
            cross18.setClickable(false);
            cross23.setClickable(false);
            cross24.setClickable(false);
            cross25.setClickable(false);
            // cloud.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        finish();
        startActivity(new Intent(mContext,Act_ImageMap.class));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cross1:
                checkUncheck(1);
                break;
            case R.id.cross2:
                checkUncheck(2);
                break;
            case R.id.cross3:
                checkUncheck(3);
                break;
            case R.id.cross4:
                checkUncheck(4);
                break;
            case R.id.cross5:
                checkUncheck(5);
                break;
            case R.id.cross6:
                checkUncheck(6);
                break;
            case R.id.cross7:
                checkUncheck(7);
                break;
            case R.id.cross8:
                checkUncheck(8);
                break;
            case R.id.cross9:
                checkUncheck(9);
                break;
            case R.id.cross10:
                checkUncheck(10);
                break;
            case R.id.cross11:
                checkUncheck(11);
                break;
            case R.id.cross12:
                checkUncheck(12);
                break;
            case R.id.cross13:
                checkUncheck(13);
                break;
            case R.id.cross14:
                checkUncheck(14);
                break;
            case R.id.cross15:
                checkUncheck(15);
                break;
            case R.id.cross16:
                checkUncheck(16);
                break;
            case R.id.cross17:
                checkUncheck(17);
                break;
            case R.id.cross18:
                checkUncheck(18);
                break;
            case R.id.cross19:
                checkUncheck(19);
                break;
            case R.id.cross20:
                checkUncheck(20);
                break;
            case R.id.cross21:
                checkUncheck(21);
                break;
            case R.id.cross22:
                checkUncheck(22);
                break;
            case R.id.cross23:
                checkUncheck(23);
                break;
            case R.id.cross24:
                checkUncheck(24);
                break;
            case R.id.cross25:
                checkUncheck(25);
                break;
            case R.id.tv_answer:
                checkAnswered();
                break;
            case R.id.red_answer_layout:
                checkAnswered();
                break;
        }
    }

    public void checkUncheck(int i) {
        if (list.get(i - 1).getCheck()) {
            list.get(i - 1).setCheck(false);
            if (i == 1) {
                cross1.setImageResource(android.R.color.transparent);
            }
            if (i == 2) {
                cross2.setImageResource(android.R.color.transparent);
            }
            if (i == 3) {
                cross3.setImageResource(android.R.color.transparent);
            }
            if (i == 4) {
                cross4.setImageResource(android.R.color.transparent);
            }
            if (i == 5) {
                cross5.setImageResource(android.R.color.transparent);
            }
            if (i == 6) {
                cross6.setImageResource(android.R.color.transparent);
            }
            if (i == 7) {
                cross7.setImageResource(android.R.color.transparent);
            }
            if (i == 8) {
                cross8.setImageResource(android.R.color.transparent);
            }
            if (i == 9) {
                cross9.setImageResource(android.R.color.transparent);
            }
            if (i == 10) {
                cross10.setImageResource(android.R.color.transparent);
            }
            if (i == 11) {
                cross11.setImageResource(android.R.color.transparent);
            }
            if (i == 12) {
                cross12.setImageResource(android.R.color.transparent);
            }
            if (i == 13) {
                cross13.setImageResource(android.R.color.transparent);
            }
            if (i == 14) {
                cross14.setImageResource(android.R.color.transparent);
            }
            if (i == 15) {
                cross15.setImageResource(android.R.color.transparent);
            }
            if (i == 16) {
                cross16.setImageResource(android.R.color.transparent);
            }
            if (i == 17) {
                cross17.setImageResource(android.R.color.transparent);
            }
            if (i == 18) {
                cross18.setImageResource(android.R.color.transparent);
            }
            if (i == 19) {
                cross19.setImageResource(android.R.color.transparent);
            }
            if (i == 20) {
                cross20.setImageResource(android.R.color.transparent);
            }
            if (i == 21) {
                cross21.setImageResource(android.R.color.transparent);
            }
            if (i == 22) {
                cross22.setImageResource(android.R.color.transparent);
            }
            if (i == 23) {
                cross23.setImageResource(android.R.color.transparent);
            }
            if (i == 24) {
                cross24.setImageResource(android.R.color.transparent);
            }
            if (i == 25) {
                cross25.setImageResource(android.R.color.transparent);
            }

        } else {
            list.get(i - 1).setCheck(true);
            if (i == 1) {
                cross1.setImageResource(R.drawable.cross);
            }
            if (i == 2) {
                cross2.setImageResource(R.drawable.cross);
            }
            if (i == 3) {
                cross3.setImageResource(R.drawable.cross);
            }
            if (i == 4) {
                cross4.setImageResource(R.drawable.cross);
            }
            if (i == 5) {
                cross5.setImageResource(R.drawable.cross);
            }
            if (i == 6) {
                cross6.setImageResource(R.drawable.cross);
            }
            if (i == 7) {
                cross7.setImageResource(R.drawable.cross);
            }
            if (i == 8) {
                cross8.setImageResource(R.drawable.cross);
            }
            if (i == 9) {
                cross9.setImageResource(R.drawable.cross);
            }
            if (i == 10) {
                cross10.setImageResource(R.drawable.cross);
            }
            if (i == 11) {
                cross11.setImageResource(R.drawable.cross);
            }
            if (i == 12) {
                cross12.setImageResource(R.drawable.cross);
            }
            if (i == 13) {
                cross13.setImageResource(R.drawable.cross);
            }
            if (i == 14) {
                cross14.setImageResource(R.drawable.cross);
            }
            if (i == 15) {
                cross15.setImageResource(R.drawable.cross);
            }
            if (i == 16) {
                cross16.setImageResource(R.drawable.cross);
            }
            if (i == 17) {
                cross17.setImageResource(R.drawable.cross);
            }
            if (i == 18) {
                cross18.setImageResource(R.drawable.cross);
            }
            if (i == 19) {
                cross19.setImageResource(R.drawable.cross);
            }
            if (i == 20) {
                cross20.setImageResource(R.drawable.cross);
            }
            if (i == 21) {
                cross21.setImageResource(R.drawable.cross);
            }
            if (i == 22) {
                cross22.setImageResource(R.drawable.cross);
            }
            if (i == 23) {
                cross23.setImageResource(R.drawable.cross);
            }
            if (i == 24) {
                cross24.setImageResource(R.drawable.cross);
            }
            if (i == 25) {
                cross25.setImageResource(R.drawable.cross);
            }

        }
        // checkAnswered();


    }

    public void checkAnswered() {
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getCheck()) {
                count++;
            }
        }
        if (list.get(0).getCheck() &&
                list.get(1).getCheck() &&
                list.get(2).getCheck() &&
                list.get(7).getCheck() && count == 4) {
            final Dialog dialog1=new Dialog(mContext);
            dialog1.requestWindowFeature(1);
            dialog1.setContentView(R.layout.dialog_missing);
            dialog1.setCanceledOnTouchOutside(true);
            ImageView imageView=dialog1.findViewById(R.id.iv_img);
            ImageView imageView2=dialog1.findViewById(R.id.iv_click);
            try{
                imageView.setImageResource(R.drawable.img_miss_red_big_edited);
            }catch (OutOfMemoryError unused) {

                Dialog dialog2=new Dialog(mContext);
                dialog1.requestWindowFeature(1);
                dialog1.setContentView(R.layout.dialog_missing);
                dialog1.setCanceledOnTouchOutside(true);
                ((ImageView) dialog2.findViewById(R.id.iv_img)).setImageResource(R.drawable.img_miss_red_big_edited);
                ((ImageView) dialog2.findViewById(R.id.iv_click)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                Window window = dialog1.getWindow();
                layoutParams.copyFrom(window.getAttributes());
                layoutParams.width = -1;
                layoutParams.height = -2;
                window.setAttributes(layoutParams);
                dialog2.show();
            }
            imageView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog1.dismiss();
                }
            });
            WindowManager.LayoutParams layoutParams=new WindowManager.LayoutParams();
            Window window1=dialog1.getWindow();
            layoutParams.copyFrom(window1.getAttributes());
            layoutParams.width=-1;
            layoutParams.height=-2;
            window1.setAttributes(layoutParams);
            //testing
            sessionParam.persist(mContext, 1);

            cross1.setClickable(false);
            cross2.setClickable(false);
            cross3.setClickable(false);
            cross4.setClickable(false);
            cross5.setClickable(false);
            cross6.setClickable(false);
            cross7.setClickable(false);
            cross8.setClickable(false);
            cross9.setClickable(false);
            cross10.setClickable(false);
            cross11.setClickable(false);
            cross12.setClickable(false);
            cross13.setClickable(false);
            cross14.setClickable(false);
            cross15.setClickable(false);
            cross16.setClickable(false);
            cross17.setClickable(false);
            cross18.setClickable(false);
            cross19.setClickable(false);
            cross20.setClickable(false);
            cross21.setClickable(false);
            cross22.setClickable(false);
            cross23.setClickable(false);
            cross24.setClickable(false);
            cross25.setClickable(false);
            cloud.setVisibility(View.GONE);


            if (sessionParam.answer1.equals("y") && sessionParam.answer2.equals("y") && sessionParam.answer3.equals("y") && sessionParam.answer4.equals("y")) {
                // startActivity(new Intent(mContext, Act_AllClear.class));
                //all 4 question clear

                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_all_answer);
                dialog.setCanceledOnTouchOutside(true);

                final ImageView iv_back = dialog.findViewById(R.id.iv_back);

                iv_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finishAllActivities();
//                        startActivity(new Intent(mContext, Act_Home.class));
                        startActivity(new Intent(mContext, Act_Home_Navigation.class));
                        dialog.dismiss();
                    }
                });

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                dialog.show();

            } else {
                // correct answer

                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                try{
                    dialog.setContentView(R.layout.dialog_clear_red);
                }catch (InflateException ofme){
                    final Dialog dialogEx = new Dialog(mContext);
                    dialogEx.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    //===================================================

                    //===================================================
                    dialogEx.setContentView(R.layout.dialog_clear_red);
                    final TextView tv_text = dialogEx.findViewById(R.id.tv_text);
                    final ImageView iv_img = dialogEx.findViewById(R.id.iv_img);
                    final RelativeLayout rl_toNext = dialogEx.findViewById(R.id.rl_toNext);
                    final TextView tv_great = dialogEx.findViewById(R.id.tv_great);
                    final TextView tv_solv_all = dialogEx.findViewById(R.id.tv_solv_all);
                    final TextView tv_toNext = dialogEx.findViewById(R.id.tv_toNext);
                    iv_img.setImageResource(R.drawable.clear_red_ninja);
                    /*tv_text.setText("Trivia of the Kaminarimon,\n" +
                            "\"Thunder Gate\"\n" +
                            "It, commonly known as Kaminarimon,\n" +
                            "is the main feature of Asakusa and\n" +
                            "officially called Furaijin-mon with\n" +
                            "a statue of Fujin enshrined on\n" +
                            "the right and that of\n" +
                            "Raijin on the left.\n" +
                            "Fujin and Raijin has enshrined for\n" +
                            "the first time when the gate\n" +
                            "was relocated to the current location\n" +
                            "after Kamakura Period.\n");*/
                    Typeface face = Typeface.createFromAsset(getAssets(),
                            "fonts/fonts.otf");
                    tv_great.setTypeface(face);
                    tv_text.setTypeface(face);
                    tv_solv_all.setTypeface(face);
                    tv_toNext.setTypeface(face);

                    rl_toNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogEx.dismiss();
                            finish();

                            startActivity(new Intent(mContext, Act_ImageMap.class));
                        }
                    });
                    dialogEx.setCanceledOnTouchOutside(true);
                    tv_great.setTypeface(face);
                    tv_text.setTypeface(face);

                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = dialogEx.getWindow();
                    lp.copyFrom(window.getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                    dialogEx.show();

                    if(!sessionParam.email.isEmpty()){
                        api_SaveAnswer();
                    }
                }//ca


                final TextView tv_text = dialog.findViewById(R.id.tv_text);
                final ImageView iv_img = dialog.findViewById(R.id.iv_img);
                final RelativeLayout rl_toNext = dialog.findViewById(R.id.rl_toNext);
                //final TextView tv_great = dialog.findViewById(R.id.tv_great);
                final TextView tv_solv_all = dialog.findViewById(R.id.tv_solv_all);
                final TextView tv_toNext = dialog.findViewById(R.id.tv_toNext);
                iv_img.setImageResource(R.drawable.clear_red_ninja);
                /*tv_text.setText("Trivia of the Kaminarimon,\n" +
                        "\"Thunder Gate\"\n" +
                        "It, commonly known as Kaminarimon,\n" +
                        "is the main feature of Asakusa and\n" +
                        "officially called Furaijin-mon with\n" +
                        "a statue of Fujin enshrined on\n" +
                        "the right and that of\n" +
                        "Raijin on the left.\n" +
                        "Fujin and Raijin has enshrined for\n" +
                        "the first time when the gate\n" +
                        "was relocated to the current location\n" +
                        "after Kamakura Period.\n");*/
                Typeface face = Typeface.createFromAsset(getAssets(),
                        "fonts/fonts.otf");
                //tv_great.setTypeface(face);
                tv_text.setTypeface(face);
                tv_solv_all.setTypeface(face);
                tv_toNext.setTypeface(face);

                rl_toNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        finish();

                        startActivity(new Intent(mContext,Act_ImageMap.class));
                        //startActivity(new Intent(mContext, Act_ImageMap.class));
                        //startActivity(new Intent(mContext, Act_QuestionTwoblue.class));
                        // startActivity(new Intent(mContext, Act_QuestiActonTwoblue.class));
                        //temp removed for testing
                       /* if(sessionParam.paymentStatus.equals("y")){
                            finishAllActivities();
                            startActivity(new Intent(mContext, Act_QuestionTwoblue.class));
                            dialog.dismiss();
                        }else{
                            finish();
                            startActivity(new Intent(mContext, Act_ImageMap.class).putExtra("path","question"));
                        }*/

                    }
                });
                dialog.setCanceledOnTouchOutside(true);
                //tv_great.setTypeface(face);
                tv_text.setTypeface(face);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                dialog.show();

                if(!sessionParam.email.isEmpty()){
                    api_SaveAnswer();
                }
            }

        } else {
            //missing answer
            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_missing);
            dialog.setCanceledOnTouchOutside(true);
            final ImageView iv_img = dialog.findViewById(R.id.iv_img);
            final ImageView iv_click = dialog.findViewById(R.id.iv_click);
            try{
                iv_img.setImageResource(R.drawable.img_miss_red_big_edited);
            }catch (OutOfMemoryError ofme){
                final Dialog dialogEx = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_missing);
                dialog.setCanceledOnTouchOutside(true);
                final ImageView iv_imgEx = dialogEx.findViewById(R.id.iv_img);
                final ImageView iv_clickEx = dialogEx.findViewById(R.id.iv_click);
                iv_imgEx.setImageResource(R.drawable.img_miss_red_big_edited);
                iv_clickEx.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // finishAllActivities();
                        //startActivity(new Intent(mContext, Act_Home.class));
                        dialog.dismiss();
                    }
                });
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialogEx.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                dialogEx.show();

            }
            iv_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // finishAllActivities();
                    //startActivity(new Intent(mContext, Act_Home.class));
                    dialog.dismiss();
                }
            });
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            dialog.show();
        }
    }

    private void api_SaveAnswer() {
        baseRequest = new BaseRequest(mContext);
        baseRequest.hideLoader();
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {

            }
            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext, message);
            }
            @Override
            public void onNetworkFailure(int requestCode, String message) {
              //  utility.showToast(mContext, getString(R.string.check_network));
            }
        });
        JsonObject object = Functions.getClient().getJsonMapObject(
                "map_name", "Asakusa",
                "email",sessionParam.email,
                "red","1",
                "blue","0",
                "green","0",
                "purple","0"
        );
        baseRequest.callAPIPostwoLoader(1, object, "index.php?method=postAnswer");
    }
}
