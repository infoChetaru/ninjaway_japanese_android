package com.bucketlist.NinjaWayJapanese.Questions;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.core.content.ContextCompat;

import com.bucketlist.NinjaWayJapanese.Map.Act_ImageMap;
import com.bucketlist.NinjaWayJapanese.ModelAnswers;
import com.bucketlist.NinjaWayJapanese.R;
import com.bucketlist.NinjaWayJapanese.Utils.BaseActivity;
import com.bucketlist.NinjaWayJapanese.Utils.SessionParam;
import com.bucketlist.NinjaWayJapanese.Utils.Utility;
import com.bucketlist.NinjaWayJapanese.retrofit.BaseRequest;
import com.bucketlist.NinjaWayJapanese.retrofit.Functions;
import com.bucketlist.NinjaWayJapanese.retrofit.RequestReciever;
import com.google.gson.JsonObject;

import java.util.ArrayList;

public class Act_QuestionFourPurple extends BaseActivity implements View.OnClickListener {
    TextView tv_welcome, tv_line2, tv_line3, tv_line4, tv_line5, tv_line6, tv_line6_1, tv_line7, tv_answer;
    ImageView cloud, iv_back;
    Typeface face;

    ImageView cross1, cross2, cross3, cross4, cross5, cross6, cross7, cross8, cross9, cross10, cross11, cross12,
            cross13, cross14, cross15, cross16, cross17, cross18, cross19, cross20, cross21, cross22, cross23, cross24, cross25;
    LinearLayout ll_answerblock,purple_answer_layout;
    ArrayList<ModelAnswers> list = new ArrayList<>();
    SessionParam sessionParam;
    private BaseRequest baseRequest;
    Utility utility;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_question_four_purple);
        if (Build.VERSION.SDK_INT >= 21) {
            // getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.dark_nav)); // Navigation bar the soft bottom of some phones like nexus and some Samsung note series
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.status_bar_purple)); //status bar or the time bar at the top
        }
        sessionParam = new SessionParam(mContext);
        for (int i = 0; i < 25; i++) {
            ModelAnswers modelAnswers = new ModelAnswers();
            modelAnswers.setCheck(false);
            list.add(modelAnswers);
        }
        utility = new Utility();
        tv_welcome = findViewById(R.id.tv_welcome);
        tv_line2 = findViewById(R.id.tv_line2);
        tv_line3 = findViewById(R.id.tv_line3);
        tv_line4 = findViewById(R.id.tv_line4);
        tv_line5 = findViewById(R.id.tv_line5);
        tv_line6 = findViewById(R.id.tv_line6);
        tv_line7 = findViewById(R.id.tv_line7);
        tv_answer = findViewById(R.id.tv_answer);
        purple_answer_layout = findViewById(R.id.purple_answer_layout);
        ll_answerblock = findViewById(R.id.ll_answerblock);
        face = Typeface.createFromAsset(getAssets(),
                "fonts/fonts.otf");
        tv_welcome.setTypeface(face);
        tv_line2.setTypeface(face);
        tv_line3.setTypeface(face);
        tv_line4.setTypeface(face);
        tv_line5.setTypeface(face);
        tv_line6.setTypeface(face);
        tv_line7.setTypeface(face);
        tv_answer.setTypeface(face);

        cloud = findViewById(R.id.cloud);
        cross1 = findViewById(R.id.cross1);
        cross2 = findViewById(R.id.cross2);
        cross3 = findViewById(R.id.cross3);
        cross4 = findViewById(R.id.cross4);
        cross5 = findViewById(R.id.cross5);
        cross6 = findViewById(R.id.cross6);
        cross7 = findViewById(R.id.cross7);
        cross8 = findViewById(R.id.cross8);
        cross9 = findViewById(R.id.cross9);
        cross10 = findViewById(R.id.cross10);
        cross11 = findViewById(R.id.cross11);
        cross12 = findViewById(R.id.cross12);
        cross13 = findViewById(R.id.cross13);
        cross14 = findViewById(R.id.cross14);
        cross15 = findViewById(R.id.cross15);
        cross16 = findViewById(R.id.cross16);
        cross17 = findViewById(R.id.cross17);
        cross18 = findViewById(R.id.cross18);
        cross19 = findViewById(R.id.cross19);
        cross20 = findViewById(R.id.cross20);
        cross21 = findViewById(R.id.cross21);
        cross22 = findViewById(R.id.cross22);
        cross23 = findViewById(R.id.cross23);
        cross24 = findViewById(R.id.cross24);
        cross25 = findViewById(R.id.cross25);
        //iv_back.setOnClickListener(this);

        if (sessionParam.answer4.equals("y")) {
            cross18.setImageResource(R.drawable.cross);
            cross23.setImageResource(R.drawable.cross);
            cross24.setImageResource(R.drawable.cross);
            cross25.setImageResource(R.drawable.cross);
            cloud.setVisibility(View.GONE);
            //change
            tv_answer.setVisibility(View.GONE);
            purple_answer_layout.setVisibility(View.GONE);
            //tv_answer.setOnClickListener(this);
            iv_back = findViewById(R.id.iv_back);
            iv_back.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    finish();
                    //startActivity(new Intent(mContext,Act_ImageMap.class));
                }
            });
        } else {
            cross1.setOnClickListener(this);
            cross2.setOnClickListener(this);
            cross3.setOnClickListener(this);
            cross4.setOnClickListener(this);
            cross5.setOnClickListener(this);
            cross6.setOnClickListener(this);
            cross7.setOnClickListener(this);
            cross8.setOnClickListener(this);
            cross9.setOnClickListener(this);
            cross10.setOnClickListener(this);
            cross11.setOnClickListener(this);
            cross12.setOnClickListener(this);
            cross13.setOnClickListener(this);
            cross14.setOnClickListener(this);
            cross15.setOnClickListener(this);
            cross16.setOnClickListener(this);
            cross17.setOnClickListener(this);
            cross18.setOnClickListener(this);
            cross19.setOnClickListener(this);
            cross20.setOnClickListener(this);
            cross21.setOnClickListener(this);
            cross22.setOnClickListener(this);
            cross23.setOnClickListener(this);
            cross24.setOnClickListener(this);
            cross25.setOnClickListener(this);
            tv_answer.setOnClickListener(this);
            purple_answer_layout.setOnClickListener(this);

            iv_back = findViewById(R.id.iv_back);
            iv_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                    //startActivity(new Intent(mContext,Act_ImageMap.class));
                }
            });
        }


        if (sessionParam.answer1.equals("y")) {
            cross1.setImageResource(R.drawable.cross_gray);
            cross2.setImageResource(R.drawable.cross_gray);
            cross3.setImageResource(R.drawable.cross_gray);
            cross8.setImageResource(R.drawable.cross_gray);
            cross1.setClickable(false);
            cross2.setClickable(false);
            cross3.setClickable(false);
            cross8.setClickable(false);
            // cloud.setVisibility(View.GONE);
        }
        if (sessionParam.answer2.equals("y")) {
            cross16.setImageResource(R.drawable.cross_gray);
            cross21.setImageResource(R.drawable.cross_gray);
            cross16.setClickable(false);

            cross21.setClickable(false);
        }
        if (sessionParam.answer3.equals("y")) {
            cross5.setImageResource(R.drawable.cross_gray);
            cross10.setImageResource(R.drawable.cross_gray);
            cross11.setImageResource(R.drawable.cross_gray);
            cross12.setImageResource(R.drawable.cross_gray);
            cross13.setImageResource(R.drawable.cross_gray);
            cross14.setImageResource(R.drawable.cross_gray);
            cross15.setImageResource(R.drawable.cross_gray);
            cross5.setClickable(false);
            cross10.setClickable(false);
            cross11.setClickable(false);
            cross12.setClickable(false);
            cross13.setClickable(false);
            cross14.setClickable(false);
            cross15.setClickable(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cross1:
                checkUncheck(1);
                break;
            case R.id.cross2:
                checkUncheck(2);
                break;
            case R.id.cross3:
                checkUncheck(3);
                break;
            case R.id.cross4:
                checkUncheck(4);
                break;
            case R.id.cross5:
                checkUncheck(5);
                break;
            case R.id.cross6:
                checkUncheck(6);
                break;
            case R.id.cross7:
                checkUncheck(7);
                break;
            case R.id.cross8:
                checkUncheck(8);
                break;
            case R.id.cross9:
                checkUncheck(9);
                break;
            case R.id.cross10:
                checkUncheck(10);
                break;
            case R.id.cross11:
                checkUncheck(11);
                break;
            case R.id.cross12:
                checkUncheck(12);
                break;
            case R.id.cross13:
                checkUncheck(13);
                break;
            case R.id.cross14:
                checkUncheck(14);
                break;
            case R.id.cross15:
                checkUncheck(15);
                break;
            case R.id.cross16:
                checkUncheck(16);
                break;
            case R.id.cross17:
                checkUncheck(17);
                break;
            case R.id.cross18:
                checkUncheck(18);
                break;
            case R.id.cross19:
                checkUncheck(19);
                break;
            case R.id.cross20:
                checkUncheck(20);
                break;
            case R.id.cross21:
                checkUncheck(21);
                break;
            case R.id.cross22:
                checkUncheck(22);
                break;
            case R.id.cross23:
                checkUncheck(23);
                break;
            case R.id.cross24:
                checkUncheck(24);
                break;
            case R.id.cross25:
                checkUncheck(25);
                break;

            case R.id.tv_answer:
                checkAnswered();
                break;
            case R.id.purple_answer_layout:
                checkAnswered();
                break;
        }

    }

    public void checkUncheck(int i) {
        if (list.get(i - 1).getCheck()) {
            list.get(i - 1).setCheck(false);
            if (i == 1) {
                cross1.setImageResource(android.R.color.transparent);
            }
            if (i == 2) {
                cross2.setImageResource(android.R.color.transparent);
            }
            if (i == 3) {
                cross3.setImageResource(android.R.color.transparent);
            }
            if (i == 4) {
                cross4.setImageResource(android.R.color.transparent);
            }
            if (i == 5) {
                cross5.setImageResource(android.R.color.transparent);
            }
            if (i == 6) {
                cross6.setImageResource(android.R.color.transparent);
            }
            if (i == 7) {
                cross7.setImageResource(android.R.color.transparent);
            }
            if (i == 8) {
                cross8.setImageResource(android.R.color.transparent);
            }
            if (i == 9) {
                cross9.setImageResource(android.R.color.transparent);
            }
            if (i == 10) {
                cross10.setImageResource(android.R.color.transparent);
            }
            if (i == 11) {
                cross11.setImageResource(android.R.color.transparent);
            }
            if (i == 12) {
                cross12.setImageResource(android.R.color.transparent);
            }
            if (i == 13) {
                cross13.setImageResource(android.R.color.transparent);
            }
            if (i == 14) {
                cross14.setImageResource(android.R.color.transparent);
            }
            if (i == 15) {
                cross15.setImageResource(android.R.color.transparent);
            }
            if (i == 16) {
                cross16.setImageResource(android.R.color.transparent);
            }
            if (i == 17) {
                cross17.setImageResource(android.R.color.transparent);
            }
            if (i == 18) {
                cross18.setImageResource(android.R.color.transparent);
            }
            if (i == 19) {
                cross19.setImageResource(android.R.color.transparent);
            }
            if (i == 20) {
                cross20.setImageResource(android.R.color.transparent);
            }
            if (i == 21) {
                cross21.setImageResource(android.R.color.transparent);
            }
            if (i == 22) {
                cross22.setImageResource(android.R.color.transparent);
            }
            if (i == 23) {
                cross23.setImageResource(android.R.color.transparent);
            }
            if (i == 24) {
                cross24.setImageResource(android.R.color.transparent);
            }
            if (i == 25) {
                cross25.setImageResource(android.R.color.transparent);
            }

        } else {
            list.get(i - 1).setCheck(true);
            if (i == 1) {
                cross1.setImageResource(R.drawable.cross);
            }
            if (i == 2) {
                cross2.setImageResource(R.drawable.cross);
            }
            if (i == 3) {
                cross3.setImageResource(R.drawable.cross);
            }
            if (i == 4) {
                cross4.setImageResource(R.drawable.cross);
            }
            if (i == 5) {
                cross5.setImageResource(R.drawable.cross);
            }
            if (i == 6) {
                cross6.setImageResource(R.drawable.cross);
            }
            if (i == 7) {
                cross7.setImageResource(R.drawable.cross);
            }
            if (i == 8) {
                cross8.setImageResource(R.drawable.cross);
            }
            if (i == 9) {
                cross9.setImageResource(R.drawable.cross);
            }
            if (i == 10) {
                cross10.setImageResource(R.drawable.cross);
            }
            if (i == 11) {
                cross11.setImageResource(R.drawable.cross);
            }
            if (i == 12) {
                cross12.setImageResource(R.drawable.cross);
            }
            if (i == 13) {
                cross13.setImageResource(R.drawable.cross);
            }
            if (i == 14) {
                cross14.setImageResource(R.drawable.cross);
            }
            if (i == 15) {
                cross15.setImageResource(R.drawable.cross);
            }
            if (i == 16) {
                cross16.setImageResource(R.drawable.cross);
            }
            if (i == 17) {
                cross17.setImageResource(R.drawable.cross);
            }
            if (i == 18) {
                cross18.setImageResource(R.drawable.cross);
            }
            if (i == 19) {
                cross19.setImageResource(R.drawable.cross);
            }
            if (i == 20) {
                cross20.setImageResource(R.drawable.cross);
            }
            if (i == 21) {
                cross21.setImageResource(R.drawable.cross);
            }
            if (i == 22) {
                cross22.setImageResource(R.drawable.cross);
            }
            if (i == 23) {
                cross23.setImageResource(R.drawable.cross);
            }
            if (i == 24) {
                cross24.setImageResource(R.drawable.cross);
            }
            if (i == 25) {
                cross25.setImageResource(R.drawable.cross);
            }
        }
        //checkAnswered();


    }

    public void checkAnswered() {
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getCheck()) {
                count++;
            }
        }

        if (list.get(17).getCheck() &&
                list.get(22).getCheck() &&
                list.get(23).getCheck() &&
                list.get(24).getCheck() &&
                count == 4) {
            sessionParam.persist(mContext, 4);
            sessionParam = new SessionParam(mContext);
            cross1.setClickable(false);
            cross2.setClickable(false);
            cross3.setClickable(false);
            cross4.setClickable(false);
            cross5.setClickable(false);
            cross6.setClickable(false);
            cross7.setClickable(false);
            cross8.setClickable(false);
            cross9.setClickable(false);
            cross10.setClickable(false);
            cross11.setClickable(false);
            cross12.setClickable(false);
            cross13.setClickable(false);
            cross14.setClickable(false);
            cross15.setClickable(false);
            cross16.setClickable(false);
            cross17.setClickable(false);
            cross18.setClickable(false);
            cross19.setClickable(false);
            cross20.setClickable(false);
            cross21.setClickable(false);
            cross22.setClickable(false);
            cross23.setClickable(false);
            cross24.setClickable(false);
            cross25.setClickable(false);
            cloud.setVisibility(View.GONE);
            if (sessionParam.answer1.equals("y") && sessionParam.answer2.equals("y") && sessionParam.answer3.equals("y") && sessionParam.answer4.equals("y")) {
                // startActivity(new Intent(mContext, Act_AllClear.class));
                //all 4 question clear
               /* final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_all_answer);
                dialog.setCanceledOnTouchOutside(true);

                final ImageView iv_back = dialog.findViewById(R.id.iv_back);

                iv_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finishAllActivities();
                        startActivity(new Intent(mContext, Act_Home.class));
                        dialog.dismiss();
                    }
                });
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                dialog.show();*/

                //---------------------------------------------------
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                //dialog.setContentView(R.layout.dialog_clear);
                dialog.setContentView(R.layout.dialog_clear_purple);
                final TextView tv_text = dialog.findViewById(R.id.tv_text);
                final ImageView iv_img = dialog.findViewById(R.id.iv_img);
                final ImageView iv_answer = dialog.findViewById(R.id.iv_answer);
                final ImageView iv_arrow = dialog.findViewById(R.id.purple_clear_arrow);
                final RelativeLayout rl_toNext = dialog.findViewById(R.id.rl_toNext);
                 RelativeLayout rl_toMap = dialog.findViewById(R.id.rl_toMap);
                final TextView tv_toMap = dialog.findViewById(R.id.tv_toMap);
                final TextView tv_great = dialog.findViewById(R.id.tv_great);
                final TextView tv_solv_all = dialog.findViewById(R.id.tv_solv_all);
                final TextView tv_cloudTwo = dialog.findViewById(R.id.tv_cloudTwo);
                final TextView tv_toNext = dialog.findViewById(R.id.tv_toNext);
                final TextView tv_tapon = dialog.findViewById(R.id.tv_tapon);
                /*tv_cloudTwo.setText("Can you, who cleared\n" +
                        "the four riddles,\n" +
                        "solve the last riddle!?");*/
                iv_img.setImageResource(R.drawable.clear_purple_ninja);
               /* tv_text.setText("The guardian dogs whose\n" +
                        "role is to guard the sanctuary\n" +
                        "as a divine servant is\n" +
                        "usually enshrined as a pair and\n" +
                        "for hopes for\n" +
                        "“a good match,”\n" +
                        "“fulfillment in love,” and\n" +
                        "“harmony between married couples”\n" +
                        "as putting this guardian dogs\n" +
                        "snuggling up each other\n" +
                        "under one umbrella.");*/
                rl_toNext.setVisibility(View.GONE);
                tv_solv_all.setVisibility(View.GONE);
                iv_answer.setVisibility(View.VISIBLE);
                tv_tapon.setVisibility(View.VISIBLE);
                tv_cloudTwo.setVisibility(View.VISIBLE);
                rl_toMap.setVisibility(View.VISIBLE);
                Typeface face = Typeface.createFromAsset(getAssets(),
                        "fonts/fonts.otf");
                tv_great.setTypeface(face);
                tv_text.setTypeface(face);
                tv_solv_all.setTypeface(face);
                tv_toNext.setTypeface(face);
                tv_tapon.setTypeface(face);
                tv_toMap.setTypeface(face);
                tv_cloudTwo.setTypeface(face);

                rl_toNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //finishAllActivities();
                        startActivity(new Intent(mContext, Act_ImageMap.class));
                        dialog.dismiss();
                        finish();
                    }
                });
                iv_arrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //finishAllActivities();
                        startActivity(new Intent(mContext, Act_ImageMap.class));
                        dialog.dismiss();
                        finish();
                    }
                });
                rl_toMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //finishAllActivities();
                        startActivity(new Intent(mContext, Act_ImageMap.class));
                        dialog.dismiss();
                        finish();
                    }
                });
                iv_answer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        finish();
//                        final Dialog dialog = new Dialog(mContext);
//                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                        dialog.setContentView(R.layout.dialog_all_answer);
//                        dialog.setCanceledOnTouchOutside(true);
//                        final TextView tv_text = dialog.findViewById(R.id.tv_text);
//                        final TextView tv_toNext = dialog.findViewById(R.id.tv_toNext);
//                        final RelativeLayout rl_toNext = dialog.findViewById(R.id.rl_toNext);
//                        tv_text.setText("卍 is the map symbol for a temple in\n" +
//                                "Japan and is basically favorable sign\n" +
//                                "of Hindus and Buddhism,\n" +
//                                "It has been imported through Buddhist\n" +
//                                "scriptures from China as in clockwise\n" +
//                                "rotation, since then Japanese Buddhist\n" +
//                                "temples have adopted 卍 symbol as\n" +
//                                "auspicious signs everywhere.\n" +
//                                "How did you like it? You have solved\n" +
//                                "riddles as touching Japanese\n" +
//                                "good and old cultures.\n" +
//                                "Senso-Ji temple with 1,400 years history\n" +
//                                "and variety of installation in the spa-\n" +
//                                "cious precincts is also known as one of\n" +
//                                "the best spiritual sites in Japan where\n" +
//                                "welcomes approximately 30 million people\n" +
//                                "a year. It is obvious that your fortune\n" +
//                                "was raised up after spending time\n" +
//                                "with us around here,\n" +
//                                "There are more mysteries which\n" +
//                                "have not been solved\n" +
//                                "in Japanese mysterious histories,\n" +
//                                "I'd like you to experience and enjoy\n" +
//                                "them with your own eyes.");
//                        Typeface face = Typeface.createFromAsset(getAssets(),
//                                "fonts/fonts.otf");
//                        tv_text.setTypeface(face);
//                        tv_toNext.setTypeface(face);
//
//                        rl_toNext.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                //finishAllActivities();
//                                finish();
////                              startActivity(new Intent(mContext, Act_Home.class));
//                                //startActivity(new Intent(mContext, Act_ImageMap.class));
//                                dialog.dismiss();
//                            }
//                        });
//                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                        Window window = dialog.getWindow();
//                        lp.copyFrom(window.getAttributes());
//                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                        window.setAttributes(lp);
//                        dialog.show();
                    }
                });
                dialog.setCanceledOnTouchOutside(true);
                tv_great.setTypeface(face);
                tv_text.setTypeface(face);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                dialog.show();
            } else {
                //correct answer
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_clear_purple);
                final TextView tv_text = dialog.findViewById(R.id.tv_text);
                final ImageView iv_img = dialog.findViewById(R.id.iv_img);
                final ImageView iv_answer = dialog.findViewById(R.id.iv_answer);
                final ImageView iv_arrow = dialog.findViewById(R.id.purple_clear_arrow);
                final RelativeLayout rl_toNext = dialog.findViewById(R.id.rl_toNext);
                final TextView tv_great = dialog.findViewById(R.id.tv_great);
                final TextView tv_solv_all = dialog.findViewById(R.id.tv_solv_all);
                final TextView tv_toNext = dialog.findViewById(R.id.tv_toNext);
                final TextView tv_tapon = dialog.findViewById(R.id.tv_tapon);
                final RelativeLayout rl_toMap = dialog.findViewById(R.id.rl_toMap);
                final TextView tv_toMap = dialog.findViewById(R.id.tv_toMap);
                final TextView tv_cloudTwo = dialog.findViewById(R.id.tv_cloudTwo);
                /*tv_cloudTwo.setText("Can you, who cleared\n" +
                        "the four riddles,\n" +
                        "solve the last riddle!?");*/
                iv_img.setImageResource(R.drawable.clear_purple_ninja);
                /*tv_text.setText("The guardian dogs whose\n" +
                        "role is to guard the sanctuary\n" +
                        "as a divine servant is\n" +
                        "usually enshrined as a pair and\n" +
                        "for hopes for\n" +
                        "“a good match,”\n" +
                        "“fulfillment in love,” and\n" +
                        "“harmony between married couples”\n" +
                        "as putting this guardian dogs\n" +
                        "snuggling up each other\n" +
                        "under one umbrella.");*/
                iv_answer.setVisibility(View.VISIBLE);
                tv_tapon.setVisibility(View.VISIBLE);
                Typeface face = Typeface.createFromAsset(getAssets(),
                        "fonts/fonts.otf");
                tv_great.setTypeface(face);
                tv_text.setTypeface(face);
                tv_solv_all.setTypeface(face);
                tv_toNext.setTypeface(face);
                tv_tapon.setTypeface(face);
                tv_toMap.setTypeface(face);
                rl_toNext.setVisibility(View.GONE);
                tv_solv_all.setVisibility(View.GONE);
                iv_answer.setVisibility(View.VISIBLE);
                tv_tapon.setVisibility(View.VISIBLE);
                tv_cloudTwo.setVisibility(View.VISIBLE);
                rl_toMap.setVisibility(View.VISIBLE);

                tv_toMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //finishAllActivities();
                        //startActivity(new Intent(mContext, Act_Home_Navigation.class));
                        // startActivity(new Intent(mContext, Act_Asakusa.class));
                        finish();
                        //startActivity(new Intent(mContext, Act_ImageMap.class));
                        dialog.dismiss();
                    }
                });
                iv_arrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //finishAllActivities();
                        dialog.dismiss();
                        finish();
                        startActivity(new Intent(mContext, Act_ImageMap.class));
                        finish();
                    }
                });
                rl_toMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //finishAllActivities();
                        dialog.dismiss();
                        finish();
                        startActivity(new Intent(mContext, Act_ImageMap.class));
                        finish();

                    }
                });

                rl_toNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //finishAllActivities();
                        startActivity(new Intent(mContext, Act_ImageMap.class));
                        dialog.dismiss();
                        finish();
                    }
                });

                iv_answer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        finish();
//                        final Dialog dialog = new Dialog(mContext);
//                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                        dialog.setContentView(R.layout.dialog_all_answer);
//                        dialog.setCanceledOnTouchOutside(true);
//                        final ImageView iv_back = dialog.findViewById(R.id.iv_back);
//                        iv_back.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                finishAllActivities();
////                                startActivity(new Intent(mContext, Act_Home.class));
//                                startActivity(new Intent(mContext, Act_Home_Navigation.class));
//                                dialog.dismiss();
//                            }
//                        });
//                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                        Window window = dialog.getWindow();
//                        lp.copyFrom(window.getAttributes());
//                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                        window.setAttributes(lp);
//                        dialog.show();
                    }
                });
                dialog.setCanceledOnTouchOutside(true);
                tv_great.setTypeface(face);
                tv_text.setTypeface(face);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                dialog.show();
                api_SaveAnswer();
            }
        } else {
            //missing answer
            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_missing);
            dialog.setCanceledOnTouchOutside(true);
            final ImageView iv_img = dialog.findViewById(R.id.iv_img);
            final ImageView iv_click = dialog.findViewById(R.id.iv_click);
            iv_img.setImageResource(R.drawable.img_miss_purple_big);
            iv_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // finishAllActivities();
                    //startActivity(new Intent(mContext, Act_Home.class));
                    dialog.dismiss();
                }
            });
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            dialog.show();
        }
    }

    private void api_SaveAnswer() {
        baseRequest = new BaseRequest(mContext);
        baseRequest.hideLoader();
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {

            }
            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext, message);
            }
            @Override
            public void onNetworkFailure(int requestCode, String message) {
               // utility.showToast(mContext, getString(R.string.check_network));
            }
        });
        JsonObject object = Functions.getClient().getJsonMapObject(
                "map_name", "Asakusa",
                "email",sessionParam.email,
                "red","1",
                "blue","1",
                "green","1",
                "purple","1"
        );
        baseRequest.callAPIPostwoLoader(1, object, "index.php?method=updateAnswer");
    }
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
        //startActivity(new Intent(mContext,Act_ImageMap.class));
    }
}
