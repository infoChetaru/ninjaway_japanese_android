package com.bucketlist.NinjaWayJapanese.Questions;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.bucketlist.NinjaWayJapanese.App_Tour.Act_Home_Navigation;
import com.bucketlist.NinjaWayJapanese.Map.Act_ImageMap;
import com.bucketlist.NinjaWayJapanese.R;
import com.bucketlist.NinjaWayJapanese.Utils.BaseActivity;


public class Act_Asakusa extends BaseActivity {

    ImageView iv_start,iv_home_icon;
    TextView tv_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_asakusa);
        iv_start = findViewById(R.id.iv_start);
        tv_text = findViewById(R.id.tv_text);
        iv_home_icon = findViewById(R.id.iv_home_icon);

        if (Build.VERSION.SDK_INT >= 21) {
            // getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.dark_nav)); // Navigation bar the soft bottom of some phones like nexus and some Samsung note series
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.app_bar_asakusa)); //status bar or the time bar at the top
        }

        /*tv_text.setText("You received a directive immediately\n" +
                "after arriving at Asakusa.\n" +
                "\n" +
                "“Welcome to Asakusa！\n" +
                "This is a city where the distinct\n" +
                "feature of Japanese tradition and\n" +
                "culture still remain. It is located in\n" +
                "Tokyo, the center of Japan,\n" +
                "while many of good and old buildings\n" +
                " and structures still exist.\n" +
                " Our partners seem hiding in this city.\n" +
                " The clues from them have already\n" +
                " written in this scroll.　\n" +
                "\n" +
                " Find the spell as solving riddles with\n" +
                " secret codes written in this scroll.\"");*/


        android.graphics.Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/fonts.otf");
       // tv_text.setTypeface(face);
        iv_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(mContext, Act_Map.class));
                startActivity(new Intent(mContext, Act_ImageMap.class));
            }
        });


        iv_home_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(mContext, Act_Map.class));
                finishAllActivities();
                startActivity(new Intent(mContext, Act_Home_Navigation.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAllActivities();
        startActivity(new Intent(mContext, Act_Home_Navigation.class));
    }
}
