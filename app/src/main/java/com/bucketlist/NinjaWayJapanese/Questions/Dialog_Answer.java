package com.bucketlist.NinjaWayJapanese.Questions;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bucketlist.NinjaWayJapanese.R;
import com.bucketlist.NinjaWayJapanese.Utils.SessionParam;


public class Dialog_Answer {
    Context context;
    SessionParam sessionParam;

    public void Dialog_Answer(Context context, SessionParam sessionParam) {
        this.context = context;
        this.sessionParam = sessionParam;
    }

    public void Dialog_ans(Context context, String ninja) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        /*Typeface face = Typeface.createFromAsset(context.getAssets(),
                "fonts/fonts.otf");*/
        final TextView tv_text;
        if(ninja.equalsIgnoreCase("r")){
            dialog.setContentView(R.layout.dialog_clear_red);
            tv_text = dialog.findViewById(R.id.tv_text);
            final ImageView iv_img = dialog.findViewById(R.id.iv_img);
            iv_img.setImageResource(R.drawable.clear_red_ninja);
            tv_text.setText("");
        }
        else if(ninja.equalsIgnoreCase("b")){
            dialog.setContentView(R.layout.dialog_clear);
            tv_text = dialog.findViewById(R.id.tv_text);
            final ImageView iv_img = dialog.findViewById(R.id.iv_img);
            iv_img.setImageResource(R.drawable.clear_blue_ninja);
            tv_text.setText("");
        }
        else  if(ninja.equalsIgnoreCase("g")){
            dialog.setContentView(R.layout.dialog_clear_green);
            final ImageView iv_img = dialog.findViewById(R.id.iv_img);
            tv_text = dialog.findViewById(R.id.tv_text);
            //iv_img = dialog.findViewById(R.id.iv_img);
            iv_img.setImageResource(R.drawable.clear_green_ninja);
            tv_text.setText("");
        }
        else if(ninja.equalsIgnoreCase("pu")){
            dialog.setContentView(R.layout.dialog_clear_purple);
             tv_text = dialog.findViewById(R.id.tv_text);
            final ImageView iv_img = dialog.findViewById(R.id.iv_img);
            final ImageView iv_answer = dialog.findViewById(R.id.iv_answer);
            final RelativeLayout rl_toNext = dialog.findViewById(R.id.rl_toNext);
            final RelativeLayout rl_toMap = dialog.findViewById(R.id.rl_toMap);
            final TextView tv_toMap = dialog.findViewById(R.id.tv_toMap);
            final TextView tv_great = dialog.findViewById(R.id.tv_great);
            final TextView tv_solv_all = dialog.findViewById(R.id.tv_solv_all);
            final TextView tv_cloudTwo = dialog.findViewById(R.id.tv_cloudTwo);
            final TextView tv_toNext = dialog.findViewById(R.id.tv_toNext);
            final TextView tv_tapon = dialog.findViewById(R.id.tv_tapon);
            final ImageView iv_arrow = dialog.findViewById(R.id.iv_arrow);
            final ImageView iv_cloud = dialog.findViewById(R.id.iv_cloud_purple);
            tv_cloudTwo.setText("");
            iv_img.setImageResource(R.drawable.clear_purple_ninja);
            tv_text.setText("");
            rl_toNext.setVisibility(View.GONE);
            tv_solv_all.setVisibility(View.GONE);
            iv_answer.setVisibility(View.VISIBLE);
            tv_tapon.setVisibility(View.VISIBLE);
            tv_cloudTwo.setVisibility(View.VISIBLE);
            iv_cloud.setVisibility(View.VISIBLE);
            rl_toMap.setVisibility(View.VISIBLE);

            /*tv_great.setTypeface(face);
            tv_text.setTypeface(face);
            tv_solv_all.setTypeface(face);
            tv_toNext.setTypeface(face);
            tv_tapon.setTypeface(face);
            tv_toMap.setTypeface(face);
            tv_cloudTwo.setTypeface(face);*/

            rl_toNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            }); rl_toMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            iv_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            tv_toMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });
            iv_answer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });
        }
        else{
            tv_text = dialog.findViewById(R.id.tv_text);
           // tv_text.setTypeface(face);
        }
        //tv_text.setTypeface(face);
        final RelativeLayout rl_toNext = dialog.findViewById(R.id.rl_toNext);
       // final TextView tv_great = dialog.findViewById(R.id.tv_great);
        final TextView tv_solv_all = dialog.findViewById(R.id.tv_solv_all);
        final TextView tv_toNext = dialog.findViewById(R.id.tv_toNext);
       // tv_great.setTypeface(face);
        //tv_solv_all.setTypeface(face);
       // tv_toNext.setTypeface(face);
        rl_toNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
       // tv_great.setTypeface(face);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

}

