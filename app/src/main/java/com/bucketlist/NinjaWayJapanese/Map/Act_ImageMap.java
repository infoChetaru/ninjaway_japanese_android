package com.bucketlist.NinjaWayJapanese.Map;

import android.accounts.Account;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import android.util.Patterns;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import com.bucketlist.NinjaWayJapanese.Adapter.Ad_PrimaryAccounts;
import com.bucketlist.NinjaWayJapanese.App_Tour.Act_Home_Navigation;
import com.bucketlist.NinjaWayJapanese.App_Tour.Act_Splash;
import com.bucketlist.NinjaWayJapanese.Questions.Act_Asakusa;
import com.bucketlist.NinjaWayJapanese.Questions.Act_QuestionFourPurple;
import com.bucketlist.NinjaWayJapanese.Questions.Act_QuestionOneRed;
import com.bucketlist.NinjaWayJapanese.Questions.Act_QuestionThreeGreen;
import com.bucketlist.NinjaWayJapanese.Questions.Act_QuestionTwoblue;
import com.bucketlist.NinjaWayJapanese.Questions.Dialog_Answer;
import com.bucketlist.NinjaWayJapanese.R;
import com.bucketlist.NinjaWayJapanese.Utils.BaseActivity;
import com.bucketlist.NinjaWayJapanese.Utils.MarshMallowPermission;
import com.bucketlist.NinjaWayJapanese.Utils.SessionParam;
import com.bucketlist.NinjaWayJapanese.Utils.Utility;
import com.bucketlist.NinjaWayJapanese.retrofit.BaseRequest;
import com.bucketlist.NinjaWayJapanese.retrofit.Functions;
import com.bucketlist.NinjaWayJapanese.retrofit.RequestReciever;
import com.google.gson.JsonObject;
import com.otaliastudios.zoom.ZoomLayout;
import com.otaliastudios.zoom.ZoomSurfaceView;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Act_ImageMap extends BaseActivity  {
    View v_shopping, v_omikuji, v_chozusa, v_smoke, v_osaian,
            v_ninja_purple, v_ninja_green, v_ninja_blue, v_ninja_red;
    ArrayList<String> SampleArrayList;
    String[] StringArray;
    Account[] account;
    ArrayAdapter<String> arrayAdapter;
    SessionParam sessionParam;
    String paymentStatus="";
    Pattern pattern;
    TextView tv_login;
    MarshMallowPermission marshMallowPermission;
    Activity activity;
    List<SkuDetails> skuDetailsList;
    private BillingClient billingClient;
    LinearLayout ll_shopping, ll_omikuji, ll_chozuso, ll_smoke, ll_osaisen;
    RelativeLayout rela_shopping,relat_omikuji,retail_chozusa,retail_smoke;
    TextView
            tv_title_shopping, tv_shoppingtext,
            tv_title_omikuji, tv_omikuji_text,
            tv_title_chozuso, tv_chozuso_text,
            tv_title_smoke, tv_smoke_text,
            tv_title_osaisen, tv_osaisen_text,tv_reset;
    TextView
                tv_omikujiSubTitle;
    ImageView iv_close_shopping, iv_close_omikuji, iv_close_chozuso, iv_close_smoke, iv_close_osaisen, iv_swastik, iv_home_icon;
    String dialogStatus = "";
    RelativeLayout rl_main,rv_close_chozuso,rv_close_shopping,rv_close_smoke,rv_close_osaisen,rv_close_omikuji;
    private BaseRequest baseRequest;
    Utility utility;
    Stripe stripe;
    String emailHold = "";
    ImageView iv_ninja_green, iv_ninja_red, iv_ninja_blue, iv_ninja_purple;
    Animation animation;
    ZoomLayout zoomlayout;
    ZoomSurfaceView zoomSurfaceView;
    Dialog_Answer dialogDone;

    List<SkuDetails> skuDetailsListGlobal;
    String holdOne = "";

    LinearLayout ly_main_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.act_image_map);
        setContentView(R.layout.act_image_map_new);
        activity = this;
        SampleArrayList=new ArrayList<>();
        ArrayList<String> arrayList=this.SampleArrayList;
        StringArray=arrayList.toArray(new String[arrayList.size()]);
        arrayAdapter=new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,StringArray);
        setUpBillingClient();


        pattern= Patterns.EMAIL_ADDRESS;
        dialogDone = new Dialog_Answer();
        tv_login = findViewById(R.id.tv_login);
        Display display = getWindowManager().getDefaultDisplay();
        iv_swastik = findViewById(R.id.iv_swastik);
        ly_main_layout = findViewById(R.id.main_linear_layout);

       // int width  = ly_main_layout.getMeasuredWidth();
        //int height = ly_main_layout.getMeasuredHeight();
        ViewTreeObserver vto = ly_main_layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    ly_main_layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    ly_main_layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                int width  = ly_main_layout.getMeasuredWidth();
                int height = ly_main_layout.getMeasuredHeight();
                try {
                    if (width < 360 && height < 500) {

                      //  iv_swastik.setPadding(0, 0, 0, 40);
                       /* ImageView image = new ImageView(Act_ImageMap.this);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 100);
                        params.setMargins(1, 1, 1, 20);
                        iv_swastik.setLayoutParams(params);*/
                        ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) iv_swastik.getLayoutParams();
                        marginParams.setMargins(2, 1, 1, 20);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });


       /* ly_main_layout.getLayoutParams().height=240;
        ly_main_layout.getLayoutParams().width=89;*/

        tv_reset = findViewById(R.id.tv_reset);
        rl_main = findViewById(R.id.rl_main);
        iv_ninja_green = findViewById(R.id.iv_ninja_green);
        iv_ninja_red = findViewById(R.id.iv_ninja_red);
        iv_ninja_blue = findViewById(R.id.iv_ninja_blue);
        iv_ninja_purple = findViewById(R.id.iv_ninja_purple);
        iv_home_icon = findViewById(R.id.iv_home_icon);

        zoomlayout = findViewById(R.id.zoomlayout);


        rv_close_chozuso = findViewById(R.id.rv_close_chozuso);
        rv_close_shopping = findViewById(R.id.rv_close_shopping);
        rv_close_smoke = findViewById(R.id.rv_close_smoke);
        rv_close_osaisen = findViewById(R.id.rv_close_osaisen);
        rv_close_omikuji = findViewById(R.id.rv_close_omikuji);


        //zoomSurfaceView = findViewById(R.id.zoomlayout);
        //zoomlayout.setAlignment();
       /* face = Typeface.createFromAsset(getAssets(),
                "fonts/fonts.otf");
        tv_login.setTypeface(face);
        tv_reset.setTypeface(face);*/
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  dialog_login();
                if (checkIfAlreadyhavePermissionForAccounts()||checkIfAlreadyhavePermissionForContacts()||checkIfAlreadyhavePermissionForStates()){
                    ActivityCompat.requestPermissions(Act_ImageMap.this,new String[]{"android.permission.GET_ACCOUNTS","android.permission.READ_CONTACTS","android.permission.READ_PHONE_STATE"},101);
                    return;
                }
                dialogPrimaryAccount("login");
            }
        });
        tv_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionParam.clearPreferences(mContext);
                finishAllActivities();
                startActivity(new Intent(mContext, Act_Splash.class));
            }
        });
        sessionParam = new SessionParam(mContext);
        marshMallowPermission = new MarshMallowPermission(activity);
        utility = new Utility();
        iv_home_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAllActivities();
                startActivity(new Intent(mContext, Act_Home_Navigation.class));
            }
        });
        if (Build.VERSION.SDK_INT >= 21) {
            // getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.dark_nav)); // Navigation bar the soft bottom of some phones like nexus and some Samsung note series
            getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.app_bar_imgmap)); //status bar or the time bar at the top
        }
//        if (sessionParam.paymentStatus.equals("y")) {
//            iv_ninja_red.setImageResource(R.drawable.marker_ninja_red_paid);
//            iv_ninja_blue.setImageResource(R.drawable.marker_ninja_blue_paid);
//            iv_ninja_green.setImageResource(R.drawable.marker_ninja_green_paid);
//            iv_ninja_purple.setImageResource(R.drawable.marker_ninja_purple_paid);
//            iv_swastik.setVisibility(View.VISIBLE);
//        } else {
//            iv_swastik.setVisibility(View.GONE);
//        }

        if (!sessionParam.email.isEmpty()) {
            tv_login.setText(sessionParam.email);
            api_checkPayment(sessionParam.email);
        }

        v_shopping = findViewById(R.id.v_shopping);
        v_omikuji = findViewById(R.id.v_omikuji);
        v_chozusa = findViewById(R.id.v_chozusa);
        v_smoke = findViewById(R.id.v_smoke);
        v_osaian = findViewById(R.id.v_osaian);

        v_ninja_purple = findViewById(R.id.v_ninja_purple);
        v_ninja_green = findViewById(R.id.v_ninja_green);
        v_ninja_blue = findViewById(R.id.v_ninja_blue);
        v_ninja_red = findViewById(R.id.v_ninja_red);

        rela_shopping=findViewById(R.id.rela_shopping);
        relat_omikuji=findViewById(R.id.relat_omikuji);
        retail_chozusa=findViewById(R.id.retail_chozusa);
        retail_smoke=findViewById(R.id.retail_smoke);
        rela_shopping=findViewById(R.id.rela_shopping);

        ll_shopping = findViewById(R.id.ll_shopping);
        tv_title_shopping = findViewById(R.id.tv_title_shopping);
        tv_shoppingtext = findViewById(R.id.tv_shoppingtext);

        ll_omikuji = findViewById(R.id.ll_omikuji);
        tv_title_omikuji = findViewById(R.id.tv_title_omikuji);
        tv_omikuji_text = findViewById(R.id.tv_omikuji_text);

        ll_chozuso = findViewById(R.id.ll_chozuso);
        tv_title_chozuso = findViewById(R.id.tv_title_chozuso);
        tv_chozuso_text = findViewById(R.id.tv_chozuso_text);

        ll_smoke = findViewById(R.id.ll_smoke);
        tv_title_smoke = findViewById(R.id.tv_title_smoke);
        tv_smoke_text = findViewById(R.id.tv_smoke_text);

        ll_osaisen = findViewById(R.id.ll_osaisen);
        tv_title_osaisen = findViewById(R.id.tv_title_osaisen);
        tv_osaisen_text = findViewById(R.id.tv_osaisen_text);

        iv_close_shopping = findViewById(R.id.iv_close_shopping);
        rv_close_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_shopping.setVisibility(View.GONE);
            }
        });

        iv_close_omikuji = findViewById(R.id.iv_close_omikuji);
        rv_close_omikuji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_omikuji.setVisibility(View.GONE);
            }
        });

        iv_close_chozuso = findViewById(R.id.iv_close_chozuso);
        rv_close_chozuso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_chozuso.setVisibility(View.GONE);
            }
        });

        iv_close_smoke = findViewById(R.id.iv_close_smoke);
        rv_close_smoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_smoke.setVisibility(View.GONE);
            }
        });

        iv_close_osaisen = findViewById(R.id.iv_close_osaisen);
        rv_close_osaisen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_osaisen.setVisibility(View.GONE);
            }
        });

        ll_shopping.setVisibility(View.GONE);


        iv_swastik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_all_answer);
                dialog.setCanceledOnTouchOutside(true);
                final ImageView iv_back = dialog.findViewById(R.id.iv_back);
                final TextView tv_text = dialog.findViewById(R.id.tv_text);
                final TextView tv_toNext = dialog.findViewById(R.id.tv_toNext);
                final RelativeLayout rl_toNext = dialog.findViewById(R.id.rl_toNext);
                /*tv_text.setText("卍 is the map symbol for a temple in\n" +
                        "Japan and is basically favorable sign\n" +
                        "of Hindus and Buddhism,\n" +
                        "It has been imported through Buddhist\n" +
                        "scriptures from China as in clockwise\n" +
                        "rotation, since then Japanese Buddhist\n" +
                        "temples have adopted 卍 symbol as\n" +
                        "auspicious signs everywhere.\n" +
                        "How did you like it? You have solved\n" +
                        "riddles as touching Japanese\n" +
                        "good and old cultures.\n" +
                        "Senso-Ji temple with 1,400 years history\n" +
                        "and variety of installation in the spa-\n" +
                        "cious precincts is also known as one of\n" +
                        "the best spiritual sites in Japan where\n" +
                        "welcomes approximately 30 million people\n" +
                        "a year. It is obvious that your fortune\n" +
                        "was raised up after spending time\n" +
                        "with us around here,\n" +
                        "There are more mysteries which\n" +
                        "have not been solved\n" +
                        "in Japanese mysterious histories,\n" +
                        "I'd like you to experience and enjoy\n" +
                        "them with your own eyes.");*/
                Typeface face = Typeface.createFromAsset(getAssets(),
                        "fonts/fonts.otf");
                tv_text.setTypeface(face);
                tv_toNext.setTypeface(face);
                rl_toNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finishAllActivities();
                        startActivity(new Intent(mContext, Act_Home_Navigation.class));
                    }
                });
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                dialog.show();
            }
        });
        blink();
        stripe = new Stripe(mContext, "pk_test_DInIGtLxTjj3lGi8v7LlnByV");



       /* v_shopping.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //dialogCheck(Integer.parseInt(String.valueOf(event.getX())),Integer.parseInt(String.valueOf(event.getY())));
                //dialogCheck(Integer.parseInt(String.valueOf(event.getX())),Integer.parseInt(String.valueOf(event.getY())));
               *//* Toast.makeText(activity, "x axis "+String.valueOf(event.getX())+"\n"+"y axis "+String.valueOf(event.getY()), Toast.LENGTH_SHORT).show();
                dialogCheck(54,36);*//*
                return true;
            }
        });*/

        //conditons changed
        if (sessionParam.answer1.equals("y") && sessionParam.answer2.equals("y") && sessionParam.answer3.equals("y") && sessionParam.answer4.equals("y")) {
            iv_swastik.setVisibility(View.VISIBLE);
        }else {
            iv_swastik.setVisibility(View.GONE);
        }

//        if (sessionParam.answer1.equals("y") && sessionParam.answer2.equals("y") && sessionParam.answer3.equals("y") && sessionParam.answer4.equals("y")) {
//            // startActivity(new Intent(mContext, Act_AllClear.class));
//
//            final Dialog dialog = new Dialog(mContext);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.setContentView(R.layout.dialog_all_answer);
//            dialog.setCanceledOnTouchOutside(true);
//
//            final ImageView iv_back = dialog.findViewById(R.id.iv_back);
//
//            iv_back.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    dialog.dismiss();
//                }
//            });
//
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = dialog.getWindow();
//            lp.copyFrom(window.getAttributes());
//            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
//            dialog.show();
//        }
        //startActivity(new Intent(mContext, Act_Map.class));

        v_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogStatus = "y";
                ll_shopping.setVisibility(View.VISIBLE);
                // tv_title_shopping.setText("Shopping Street");
                //tv_shoppingtext.setText("Enjoy Japanese traditional souvenirs and foods!");
               // tv_title_shopping.setText("商店街");
                //tv_shoppingtext.setText("日本の伝統的なお土産や食べ物があるから楽しんで！");
               // tv_title_shopping.setTypeface(face);
                //tv_shoppingtext.setTypeface(face);
                ll_omikuji.setVisibility(View.GONE);
                ll_chozuso.setVisibility(View.GONE);
                ll_smoke.setVisibility(View.GONE);
                ll_osaisen.setVisibility(View.GONE);

            }
        });
        rela_shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogStatus = "y";
                ll_shopping.setVisibility(View.VISIBLE);
                // tv_title_shopping.setText("Shopping Street");
                //tv_shoppingtext.setText("Enjoy Japanese traditional souvenirs and foods!");
                // tv_title_shopping.setText("商店街");
                //tv_shoppingtext.setText("日本の伝統的なお土産や食べ物があるから楽しんで！");
                //tv_title_shopping.setTypeface(face);
                //tv_shoppingtext.setTypeface(face);
                ll_omikuji.setVisibility(View.GONE);
                ll_chozuso.setVisibility(View.GONE);
                ll_smoke.setVisibility(View.GONE);
                ll_osaisen.setVisibility(View.GONE);
            }
        });


        v_omikuji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogStatus = "y";
                ll_omikuji.setVisibility(View.VISIBLE);
               // tv_title_omikuji.setText("Omikuji");
                tv_title_omikuji.setText("おみくじ");
//              tv_omikuji_text.setText("Omikuji is a sacred lots drawn\nat temples or shrines to tell\none's fortune. Its origin is said\nthat Omikuji was used to be\n" +
//                        "utilized for divination of divine\nintention to make important\ndecisions about religion and\npolitic and choose a successor.");
              /*  tv_omikuji_text.setText("Omikuji is a sacred lots drawn at temples or shrines to tell one's fortune. Its origin is said that Omikuji was used to be" +
                        "utilized for divination of divine intention to make important decisions about religion and politic and choose a successor.");
              */
               //  tv_omikuji_text.setText("神社・仏閣等で吉凶を占うために行う籤（くじ）のこと");
                //tv_omikujiSubTitle.setText("神社・仏閣等で吉凶を占うために行う籤（くじ）のこと");
              //tv_title_omikuji.setTypeface(face);
               // tv_omikuji_text.setTypeface(face);
                ll_shopping.setVisibility(View.GONE);
                ll_chozuso.setVisibility(View.GONE);
                ll_smoke.setVisibility(View.GONE);
                ll_osaisen.setVisibility(View.GONE);
            }
        });
        relat_omikuji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogStatus = "y";
                ll_omikuji.setVisibility(View.VISIBLE);
                // tv_title_omikuji.setText("Omikuji");
                tv_title_omikuji.setText("おみくじ");
//              tv_omikuji_text.setText("Omikuji is a sacred lots drawn\nat temples or shrines to tell\none's fortune. Its origin is said\nthat Omikuji was used to be\n" +
//                        "utilized for divination of divine\nintention to make important\ndecisions about religion and\npolitic and choose a successor.");
              /*  tv_omikuji_text.setText("Omikuji is a sacred lots drawn at temples or shrines to tell one's fortune. Its origin is said that Omikuji was used to be" +
                        "utilized for divination of divine intention to make important decisions about religion and politic and choose a successor.");
              */
                //  tv_omikuji_text.setText("神社・仏閣等で吉凶を占うために行う籤（くじ）のこと");
                //tv_omikujiSubTitle.setText("神社・仏閣等で吉凶を占うために行う籤（くじ）のこと");
                //tv_title_omikuji.setTypeface(face);
                // tv_omikuji_text.setTypeface(face);
                ll_shopping.setVisibility(View.GONE);
                ll_chozuso.setVisibility(View.GONE);
                ll_smoke.setVisibility(View.GONE);
                ll_osaisen.setVisibility(View.GONE);
            }
        });


        retail_chozusa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogStatus = "y";
                ll_chozuso.setVisibility(View.VISIBLE);
                //tv_title_chozuso.setText("Chozusha");
                tv_title_chozuso.setText("手水舎");
//              tv_chozuso_text.setText("Chozusha is a building for visi-" +
//                        "\ntors to purify their spirits with" +
//                        "\nwater before making a prayer at" +
//                        "\nthe shrine. It cleans mind and" +
//                        "\nbody as cleaning hands and" +
//                        "\nrinsing mouth with water and\n" +
//                        "washing off carnal desires in\n" +
//                        "order to visit Buddha. It is for" +
//                        "\nvisiting spiritual place after\n" +
//                        "expelling evil spirit and wash\n" +
//                        "sin clean");
                // tv_chozuso_text.setText("Chozusha is a building for visitors to purify their spirits with water before making a prayer at the shrine. It cleans mind and body as cleaning hands and rinsing mouth with water and washing off carnal desires in order to visit Buddha. It is for visiting spiritual place and wash sin clean.");
                //tv_title_chozuso.setTypeface(face);
                // tv_chozuso_text.setTypeface(face);
                ll_shopping.setVisibility(View.GONE);
                ll_smoke.setVisibility(View.GONE);
                ll_osaisen.setVisibility(View.GONE);
                ll_omikuji.setVisibility(View.GONE);
            }
        });
        v_chozusa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogStatus = "y";
                ll_chozuso.setVisibility(View.VISIBLE);
                //tv_title_chozuso.setText("Chozusha");
                tv_title_chozuso.setText("手水舎");
//              tv_chozuso_text.setText("Chozusha is a building for visi-" +
//                        "\ntors to purify their spirits with" +
//                        "\nwater before making a prayer at" +
//                        "\nthe shrine. It cleans mind and" +
//                        "\nbody as cleaning hands and" +
//                        "\nrinsing mouth with water and\n" +
//                        "washing off carnal desires in\n" +
//                        "order to visit Buddha. It is for" +
//                        "\nvisiting spiritual place after\n" +
//                        "expelling evil spirit and wash\n" +
//                        "sin clean");
               // tv_chozuso_text.setText("Chozusha is a building for visitors to purify their spirits with water before making a prayer at the shrine. It cleans mind and body as cleaning hands and rinsing mouth with water and washing off carnal desires in order to visit Buddha. It is for visiting spiritual place and wash sin clean.");
                //tv_title_chozuso.setTypeface(face);
               // tv_chozuso_text.setTypeface(face);
                ll_shopping.setVisibility(View.GONE);
                ll_smoke.setVisibility(View.GONE);
                ll_osaisen.setVisibility(View.GONE);
                ll_omikuji.setVisibility(View.GONE);
            }
        });


        retail_smoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogStatus = "y";
                ll_smoke.setVisibility(View.VISIBLE);
                // tv_title_smoke.setText("Smoke");
                //old code
//                tv_smoke_text.setText("Meaning of smoke" +
//                        "\nThe smoke you take on your" +
//                        "\nbody is arisen from incense sticks" +
//                        "\noffered in a huge pottery, called" +
//                        "\n\"Jo-ko-ro\" It is one of" +
//                        "\nBuddhist altar" +
//                        "\nequipment to cleanse visitors'\n" +
//                        "body. After a long period, it is+" +
//                        "\nsaid that the smoke from the" +
//                        "\npottery heals the condition if" +
//                        "\ntaken on the part of body." +
//                        "\nIncense sticks can be purchased" +
//                        "\nat a store by the pottery.");
                // tv_smoke_text.setText("Meaning of smoke The smoke you take on your body is arisen from incense sticks offered in a huge pottery, called \"Jo-ko-ro\" It is one of Buddhist altar equipment to cleanse visitor's body. After a long period, it is said that the smoke from the pottery heals the condition if taken on the part of body. Incense sticks can be purchased at a store by the pottery.");
                //tv_title_smoke.setTypeface(face);
                //tv_smoke_text.setTypeface(face);
                ll_shopping.setVisibility(View.GONE);
                ll_chozuso.setVisibility(View.GONE);
                ll_omikuji.setVisibility(View.GONE);
                ll_osaisen.setVisibility(View.GONE);
            }
        });
        v_smoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogStatus = "y";
                ll_smoke.setVisibility(View.VISIBLE);
               // tv_title_smoke.setText("Smoke");
                //old code
//                tv_smoke_text.setText("Meaning of smoke" +
//                        "\nThe smoke you take on your" +
//                        "\nbody is arisen from incense sticks" +
//                        "\noffered in a huge pottery, called" +
//                        "\n\"Jo-ko-ro\" It is one of" +
//                        "\nBuddhist altar" +
//                        "\nequipment to cleanse visitors'\n" +
//                        "body. After a long period, it is+" +
//                        "\nsaid that the smoke from the" +
//                        "\npottery heals the condition if" +
//                        "\ntaken on the part of body." +
//                        "\nIncense sticks can be purchased" +
//                        "\nat a store by the pottery.");
               // tv_smoke_text.setText("Meaning of smoke The smoke you take on your body is arisen from incense sticks offered in a huge pottery, called \"Jo-ko-ro\" It is one of Buddhist altar equipment to cleanse visitor's body. After a long period, it is said that the smoke from the pottery heals the condition if taken on the part of body. Incense sticks can be purchased at a store by the pottery.");
                //tv_title_smoke.setTypeface(face);
                //tv_smoke_text.setTypeface(face);
                ll_shopping.setVisibility(View.GONE);
                ll_chozuso.setVisibility(View.GONE);
                ll_omikuji.setVisibility(View.GONE);
                ll_osaisen.setVisibility(View.GONE);
            }
        });

        v_osaian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogStatus = "y";
                ll_osaisen.setVisibility(View.VISIBLE);
                tv_title_osaisen.setText("お賽銭");
//                tv_osaisen_text.setText("Osaisen is an offering,\n" +
//                        "not for praying, but for worship\n" +
//                        "as thanks of fulfillment to\n" +
//                        "Buddha. It is common to through\n" +
//                        "5 Japanese Yen(Goen) for goo\n" +
//                        "omen(Goen).\nAmount wouldn't be a problem.\nIt is important to offer a pray.\nMay you have a Goen");
                //tv_osaisen_text.setText("Osaisen is an offering, not for praying, but for worship as thanks of fulfillment to Buddha. It is common to through 5 Japanese Yen(Goen) for good men(Goen). Amount wouldn't be a problem. It is important to offer a pray. May you have a Goen.");
                //tv_title_osaisen.setTypeface(face);
                //tv_osaisen_text.setTypeface(face);
                ll_shopping.setVisibility(View.GONE);
                ll_chozuso.setVisibility(View.GONE);
                ll_omikuji.setVisibility(View.GONE);
                ll_smoke.setVisibility(View.GONE);
            }
        });

        iv_ninja_purple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sessionParam=new SessionParam(mContext);
                if (!sessionParam.answer3.equals("y")){
                    showDialog(mContext,"Green");
                }else if (sessionParam.answer4.equalsIgnoreCase("y")){
                    dialogDone=new Dialog_Answer();
                    dialogDone.Dialog_ans(mContext,"pu");
                }else {
                    startActivity(new Intent(mContext, Act_QuestionFourPurple.class));
                }
            }
        });
        iv_ninja_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionParam=new SessionParam(mContext);
                if (!sessionParam.answer2.equals("y")){
                    showDialog(mContext,"Blue");
                }else if (sessionParam.answer3.equalsIgnoreCase("y")){
                    dialogDone=new Dialog_Answer();
                    dialogDone.Dialog_ans(mContext,"g");
                }else {
                    startActivity(new Intent(mContext, Act_QuestionThreeGreen.class));
                }
            }
        });


        iv_ninja_blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionParam=new SessionParam(mContext);
                if (!sessionParam.answer1.equals("y")){
                    showDialog(mContext,"Red");
                }/*else {
                    startActivity(new Intent(mContext,Act_QuestionTwoblue.class));
                }*/
                else if (!sessionParam.paymentStatus.equalsIgnoreCase("y")){
                    initiatePayment(false);
                }else if (sessionParam.answer2.equalsIgnoreCase("y")){
                    dialogDone.Dialog_ans(mContext,"b");
                }else {
                    sessionParam=new SessionParam(mContext);
                    if (sessionParam.paymentStatus.equals("y")){
                        startActivity(new Intent(mContext, Act_QuestionTwoblue.class));
                    }
                    initiatePayment(false);
                }
            }
        });

        iv_ninja_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionParam.answer1.equalsIgnoreCase("y")){
                    dialogDone.Dialog_ans(mContext,"r");
                    return;
                }
                finishAllActivities();
                startActivity(new Intent(mContext, Act_QuestionOneRed.class));
            }
        });
    }

    void handlePurchase(Purchase purchase) {
        // Purchase retrieved from BillingClient#queryPurchases or your PurchasesUpdatedListener.
        //Purchase purchase = ...;

        // Verify the purchase.
        // Ensure entitlement was not already granted for this purchaseToken.
        // Grant entitlement to the user.

        ConsumeParams consumeParams =
                ConsumeParams.newBuilder()
                        .setPurchaseToken(purchase.getPurchaseToken())
                        .build();

        ConsumeResponseListener listener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(BillingResult billingResult, String purchaseToken) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // Handle the success of the consume operation.
                }
            }
        };

        billingClient.consumeAsync(consumeParams, listener);
    }



    private void initiatePayment(final boolean z) {
        try {
            if (billingClient.isReady()) {
                List<String> skuList = new ArrayList<> ();
                skuList.add("ninja_3");
                skuList.add("ninja_test_5");
                SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
                billingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(@NonNull BillingResult billingResult, @Nullable List<SkuDetails> list) {
                        Act_ImageMap act_ImageMap = Act_ImageMap.this;
                        act_ImageMap.skuDetailsListGlobal = list;
                        assert list != null;
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list.size()>0) {
                        try {
                            BillingFlowParams build = BillingFlowParams.newBuilder().setSkuDetails(Act_ImageMap.this.skuDetailsListGlobal.get(0)).build();
                            if (!z) {
                                Act_ImageMap.this.billingClient.launchBillingFlow(activity, build);
                                return;
                            }
                            return;
                        }catch (Exception e){
                            e.printStackTrace();
                            return;
                        }
                            // Toast.makeText(mContext, "cannot load product", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                /*billingClient.querySkuDetailsAsync(SkuDetailsParams.newBuilder().setSkusList(Arrays.asList(new String[]{"ninja_test_5"})).setType(BillingClient.SkuType.INAPP).build(), new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(int i, List<SkuDetails> list) {
                        Act_ImageMap act_ImageMap = Act_ImageMap.this;
                        act_ImageMap.skuDetailsListGlobal = list;
                        if (i == 0) {
                            BillingFlowParams build = BillingFlowParams.newBuilder().setSkuDetails(Act_ImageMap.this.skuDetailsListGlobal.get(0)).build();
                            if (!z) {
                                Act_ImageMap.this.billingClient.launchBillingFlow(activity, build);
                                return;
                            }
                            return;
                            // Toast.makeText(mContext, "cannot load product", Toast.LENGTH_SHORT).show();
                        }
                    }
                });*/
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private PurchasesUpdatedListener purchaseUpdateListener = new PurchasesUpdatedListener() {
        @Override
        public void onPurchasesUpdated(BillingResult billingResult, List<Purchase> purchases) {
            // To be implemented in a later section.
          if (purchases != null) {

                  if(billingResult.getResponseCode()== BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED){
                      //  Toast.makeText(activity, "Item already owned, kindly check login email", Toast.LENGTH_SHORT).show();
                      return;
                  }if(billingResult.getResponseCode()== BillingClient.BillingResponseCode.USER_CANCELED){
                  //  Toast.makeText(activity, "Payment Caneled", Toast.LENGTH_SHORT).show();
                  return;
                     }
              for (Purchase purchase : purchases) {
                  handlePurchase(purchase);
              }
                if(purchases.size()>0){
                    if(!purchases.get(0).getOrderId().isEmpty()){

                        iv_ninja_red.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_red_paid));
                        iv_ninja_blue.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_blue_paid));
                        iv_ninja_green.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_green_paid));
                        iv_ninja_purple.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_purple_paid));
                        sessionParam.paymentStatus(mContext, "y");
                        api_sendPaymentStatus(purchases.get(0).getOrderId());
                        //   Toast.makeText(activity, "Purchase successful", Toast.LENGTH_SHORT).show();

                    }
                }else{
                    // Toast.makeText(activity, "Purchase failed/ or already bought", Toast.LENGTH_SHORT).show();
                }

            }
        }
    };
    private void setUpBillingClient() {

        billingClient = BillingClient.newBuilder(this).setListener(purchaseUpdateListener)
                .enablePendingPurchases().build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                if(billingResult.getResponseCode()== BillingClient.BillingResponseCode.OK){
                    // Toast.makeText(activity, "connected to billing", Toast.LENGTH_SHORT).show();
                }else{
                    //Toast.makeText(activity, ""+billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {

            }
        });
       /* billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(int responseCode) {
                 if(responseCode== BillingClient.BillingResponse.OK){
                    // Toast.makeText(activity, "connected to billing", Toast.LENGTH_SHORT).show();
                 }else{
                   //Toast.makeText(activity, ""+responseCode, Toast.LENGTH_SHORT).show();
                 }
            }

            @Override
            public void onBillingServiceDisconnected() {
               // Toast.makeText(activity, "unable to connect", Toast.LENGTH_SHORT).show();
            }
        });*/
    }

   /* @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {

//        {
//            "kind": "androidpublisher#productPurchase",
//                "purchaseTimeMillis": long,
//            "purchaseState": integer,
//                "consumptionState": integer,
//                "developerPayload": string,
//                "orderId": string,
//                "purchaseType": integer,
//                "acknowledgementState": integer
//        }



        if(responseCode== BillingClient.BillingResponse.ITEM_ALREADY_OWNED){
          //  Toast.makeText(activity, "Item already owned, kindly check login email", Toast.LENGTH_SHORT).show();
            return;
        }if(responseCode== BillingClient.BillingResponse.USER_CANCELED){
          //  Toast.makeText(activity, "Payment Caneled", Toast.LENGTH_SHORT).show();
            return;
        }


        if (responseCode != BillingClient.BillingResponse.OK) {
          //  Toast.makeText(activity, "Purchase failed", Toast.LENGTH_SHORT).show();
           // purchaseFailed();
        }

        else if (purchases != null) {
            if(purchases.size()>0){
                if(!purchases.get(0).getOrderId().isEmpty()){

                    iv_ninja_red.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_red_paid));
                    iv_ninja_blue.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_blue_paid));
                    iv_ninja_green.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_green_paid));
                    iv_ninja_purple.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_purple_paid));
                    sessionParam.paymentStatus(mContext, "y");
                    api_sendPaymentStatus(purchases.get(0).getOrderId());
                 //   Toast.makeText(activity, "Purchase successful", Toast.LENGTH_SHORT).show();

                }
            }else{
               // Toast.makeText(activity, "Purchase failed/ or already bought", Toast.LENGTH_SHORT).show();
            }

        }
    }*/

    public void pantBy() {
        zoomlayout.panTo(100, 400, true);
        //zoomlayout.moveTo(2,100,200,true);
    }

    public void blink() {
        sessionParam = new SessionParam(mContext);
        animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
        animation.setDuration(500); //1 second duration for each animation cycle
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
        animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.

        if (!sessionParam.answer1.equals("y") && !sessionParam.answer2.equals("y") && !sessionParam.answer3.equals("y") && !sessionParam.answer4.equals("y")) {
            iv_ninja_red.startAnimation(animation);
        }
        if (sessionParam.answer1.equals("y") && !sessionParam.answer2.equals("y")) {
            iv_ninja_blue.startAnimation(animation);
            animation = new AlphaAnimation(1, 1);
            iv_ninja_red.startAnimation(animation);
            //zoomlayout.panBy(100, 400, true); //blue ninja
        }
        if (sessionParam.answer2.equals("y") && !sessionParam.answer3.equals("y")) {
            iv_ninja_green.startAnimation(animation);
            animation = new AlphaAnimation(1, 1);
            iv_ninja_blue.startAnimation(animation);
            //pantBy();
            //zoomlayout.panBy(300, 600, true); //green ninja
        }
        if (sessionParam.answer3.equals("y") && !sessionParam.answer4.equals("y")) {
            iv_ninja_purple.startAnimation(animation);
            animation = new AlphaAnimation(1, 1);
            iv_ninja_green.startAnimation(animation);
            // zoomlayout.panBy(-300, 600, true); //purple ninja
        }
        if (sessionParam.answer4.equals("y")) {
            animation = new AlphaAnimation(1, 1);
            iv_ninja_purple.startAnimation(animation);
            //zoomlayout.panBy(-300, 600, true); //purple ninja
        }
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        if (dialogStatus.equalsIgnoreCase("y")) {
            ll_chozuso.setVisibility(View.GONE);
            ll_omikuji.setVisibility(View.GONE);
            ll_osaisen.setVisibility(View.GONE);
            ll_smoke.setVisibility(View.GONE);
            ll_shopping.setVisibility(View.GONE);
            finish();    //-------------> hemant add this line
            startActivity(new Intent(mContext, Act_Asakusa.class));
        } else {
            finish();
            startActivity(new Intent(mContext, Act_Asakusa.class));

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //pantBy();
        try {
            blink();
            String path = getIntent().getStringExtra("path");
            if (path.equalsIgnoreCase("question")) {
              //  dialog_login();
                dialogPrimaryAccount("login");
            }
        } catch (NullPointerException npe) {
            sessionParam = new SessionParam(mContext);
            if (sessionParam.answer1.equals("y") && sessionParam.answer2.equals("y") && sessionParam.answer3.equals("y") && sessionParam.answer4.equals("y")) {
                iv_swastik.setVisibility(View.VISIBLE);
            }
        }
    }
    public void dialog_imagePreview(String title, String text) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_shops);
        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_text = dialog.findViewById(R.id.tv_text);
        tv_title.setText(title);
        tv_text.setText(text);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    public static AlertDialog showDialog(Context context, String ninja) {
         AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if(ninja.equals("Red")){
                    builder.setTitle("おっとっと！！")
                    //.setMessage("It seems you missed out the " + ninja + " Ninja!!")
                    //.setMessage("それ らしい 君 逃した でる の " + ninja + " 忍者！！")
                   // .setMessage("赤忍者を見逃したようです!!")
                    .setMessage("赤い忍者を見逃したようです!! ")
                    .setCancelable(true)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });


        }else if (ninja.equals("Blue")){
                   // builder.setTitle("おっとっと！")
                    builder.setTitle("おっとっと！！")
                    //.setMessage("It seems you missed out the " + ninja + " Ninja!!")
                    //.setMessage("それ らしい 君 逃した でる の " + ninja + " 忍者！！")
                    //.setMessage("ブルーニンジャを逃したようです!!")
                     .setMessage("青い忍者を見逃したようです!!")
                      .setCancelable(true)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });

        }else if (ninja.equals("Green")){
                    builder.setTitle("おっとっと！!")
                    //.setMessage("It seems you missed out the " + ninja + " Ninja!!")
                    //.setMessage("それ らしい 君 逃した でる の " + ninja + " 忍者！！")
                    .setMessage("緑の忍者を見逃したようです!!")
                    .setCancelable(true)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
        }

       /* final AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle("おっとっと！")
                //.setMessage("It seems you missed out the " + ninja + " Ninja!!")
                .setMessage("それ らしい 君 逃した でる の " + ninja + " 忍者！！")
                .setMessage("それ らしい 君 逃した でる の " + ninja + " 忍者！！")
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();*/
        AlertDialog alert = builder.create();
        alert.show();
        return alert;
    }

    public void dialog_login() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_email);
        ImageView iv_close = dialog.findViewById(R.id.iv_close);
        TextView tv_yen = dialog.findViewById(R.id.tv_yen);
        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_line1 = dialog.findViewById(R.id.tv_line1);
        TextView tv_line2 = dialog.findViewById(R.id.tv_line2);
        final EditText et_email = dialog.findViewById(R.id.et_email);
        TextView tv_proceed = dialog.findViewById(R.id.tv_proceed);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/fonts.otf");
        tv_yen.setTypeface(face);
        tv_line1.setTypeface(face);
        tv_line2.setTypeface(face);
        tv_proceed.setTypeface(face);
        tv_title.setTypeface(face);

        sessionParam = new SessionParam(mContext);
        et_email.setText(sessionParam.email);
        tv_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_email.getText().toString().isEmpty()) {
                   // Toast.makeText(mContext, "Please enter email", Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(!Utility.isValidEmail(et_email.getText().toString())){
                  //  Toast.makeText(mContext, "Please enter valid email", Toast.LENGTH_SHORT).show();
                    return;
                }

                else {
                    api_addEmail(et_email.getText().toString(), "login");
//                    tv_login.setText(et_email.getText().toString());
//                    sessionParam.saveEmail(mContext, et_email.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    public void dialog_askPayment() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_amount);
        ImageView iv_close = dialog.findViewById(R.id.iv_close);
        final CardInputWidget card_input_widget = dialog.findViewById(R.id.card_input_widget);
        TextView tv_yen = dialog.findViewById(R.id.tv_yen);
        TextView tv_line1 = dialog.findViewById(R.id.tv_line1);
        TextView tv_line2 = dialog.findViewById(R.id.tv_line2);
        final EditText et_email = dialog.findViewById(R.id.et_email);
        final TextView tv_proceed = dialog.findViewById(R.id.tv_proceed);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/fonts.otf");
        tv_yen.setTypeface(face);
        tv_line1.setTypeface(face);
        tv_line2.setTypeface(face);
        tv_proceed.setTypeface(face);
        et_email.setText(sessionParam.email);
        tv_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv_proceed.getText().toString().equalsIgnoreCase("proceed")) {
                    if (et_email.getText().toString().isEmpty()) {
                       // Toast.makeText(mContext, "Please enter email", Toast.LENGTH_SHORT).show();

                    } else {
                        emailHold = et_email.getText().toString();
                        et_email.setVisibility(View.GONE);
                        card_input_widget.setVisibility(View.VISIBLE);
                        tv_proceed.setText("Confirm");

                       /* api_addEmail(et_email.getText().toString(),"payment");
                        tv_login.setText(et_email.getText().toString());
                        sessionParam.saveEmail(mContext, et_email.getText().toString());
                        dialog.dismiss();*/
                    }
                } else {
                    Card card = card_input_widget.getCard();
                    if (card != null) {
                        dialog.dismiss();
                       // utility.showToast(mContext, "Please wait");
                        stripe.createToken(new Card(card.getNumber(), card.getExpMonth(), card.getExpYear(), card.getCVC()),
                                new TokenCallback() {
                                    public void onSuccess(Token token) {
                                        // Send token to your own web service
                                        api_MakePayment(emailHold, token.getId().toString());
                                    }
                                    public void onError(Exception error) {
                                       /* Toast.makeText(mContext,
                                                error.getLocalizedMessage(),
                                                Toast.LENGTH_LONG).show();*/
                                        baseRequest.hideLoader();
                                    }
                                });
//                        //confirmPaymentIntent(card);
                    } else {
                        //utility.showToast(mContext, "Invalid card details");
                    }
                }
            }
        });
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    private void api_addEmail(final String email, final String path) {

        baseRequest = new BaseRequest(mContext);
        baseRequest.hideLoader();
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    JSONObject jsonObject = new JSONObject(object.toString());
                    JSONArray transArray = new JSONArray();
                    JSONArray answerArray = new JSONArray();
                    transArray = jsonObject.getJSONArray("transactions");
                    answerArray = jsonObject.getJSONArray("answers");
                    if (answerArray.length() > 0) {
                        JSONObject answerObject = answerArray.getJSONObject(0);
                        if (answerObject.optString("red").equalsIgnoreCase("1")
                                && answerObject.optString("blue").equalsIgnoreCase("1")
                                && answerObject.optString("green").equalsIgnoreCase("1")
                                && answerObject.optString("purple").equalsIgnoreCase("1")) {
                            sessionParam.persist(mContext, 1);
                            sessionParam.persist(mContext, 2);
                            sessionParam.persist(mContext, 3);
                            sessionParam.persist(mContext, 4);
                            final Dialog dialog = new Dialog(mContext);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_all_answer);
                            dialog.setCanceledOnTouchOutside(true);
                            final ImageView iv_back = dialog.findViewById(R.id.iv_back);
                            iv_back.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = dialog.getWindow();
                            lp.copyFrom(window.getAttributes());
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                            dialog.show();
                        } else {

                            if(sessionParam.email.isEmpty()&&sessionParam.answer1.equals("y")){
                                //then do nothing
                                holdOne = "y";


                            }else{
                                if (answerObject.optString("red").equalsIgnoreCase("1")) {
                                    sessionParam.persist(mContext, 1);
                                } else {
                                    sessionParam.redOne(mContext);
                                }
                            }

                            if (answerObject.optString("blue").equalsIgnoreCase("1")) {
                                sessionParam.persist(mContext, 2);
                            } else {
                                sessionParam.blueOne(mContext);
                            }
                            if (answerObject.optString("green").equalsIgnoreCase("1")) {
                                sessionParam.persist(mContext, 3);
                            } else {
                                sessionParam.greenOne(mContext);
                            }
                            if (answerObject.optString("purple").equalsIgnoreCase("1")) {
                                sessionParam.persist(mContext, 4);
                            } else {
                                sessionParam.purpleOne(mContext);
                            }
                        }
                        if (!sessionParam.answer1.equals("y") && !sessionParam.answer2.equals("y") && !sessionParam.answer3.equals("y") && !sessionParam.answer4.equals("y")) {
                            iv_ninja_red.startAnimation(animation);
                        }
                        if (sessionParam.answer1.equals("y") && !sessionParam.answer2.equals("y")) {
                            iv_ninja_blue.startAnimation(animation);
                        }
                        if (sessionParam.answer2.equals("y") && !sessionParam.answer3.equals("y")) {
                            iv_ninja_purple.startAnimation(animation);
                        }
                        if (sessionParam.answer3.equals("y") && !sessionParam.answer4.equals("y")) {
                            iv_ninja_purple.startAnimation(animation);
                        }
                        /*else if (!sessionParam.answer1.isEmpty()) {
                            sessionParam.persist(mContext, 1);
                        }
                        if (!answerObject.optString("blue").equalsIgnoreCase("1")) {
                            sessionParam.persist(mContext, 2);
                        } else {
                            sessionParam.blueOne(mContext);
                        }
                        if (!answerObject.optString("green").equalsIgnoreCase("1")) {
                            sessionParam.persist(mContext, 3);
                        } else {
                            sessionParam.greenOne(mContext);
                        }
                        if (!answerObject.optString("purple").equalsIgnoreCase("1")) {
                            sessionParam.persist(mContext, 4);
                        } else {
                            sessionParam.purpleOne(mContext);
                        }*/
                    }
                    if (transArray.length() > 0) {
                        sessionParam.paymentStatus(mContext, "y");
                        sessionParam.saveEmail(mContext, email);
                        tv_login.setText(email);
                        //paid condition
                        iv_ninja_red.setImageResource(R.drawable.marker_ninja_red_paid);
                        iv_ninja_blue.setImageResource(R.drawable.marker_ninja_blue_paid);
                        iv_ninja_green.setImageResource(R.drawable.marker_ninja_green_paid);
                        iv_ninja_purple.setImageResource(R.drawable.marker_ninja_purple_paid);
                        //rl_main.setBackgroundResource(R.mipmap.bg_map_paid);
                    } else {
                        sessionParam.paymentStatus(mContext, "");
                        sessionParam.saveEmail(mContext, email);
                        iv_ninja_red.setImageResource(R.drawable.marker_ninja_red);
                        iv_ninja_blue.setImageResource(R.drawable.marker_ninja_blue);
                        iv_ninja_green.setImageResource(R.drawable.marker_ninja_green);
                        iv_ninja_purple.setImageResource(R.drawable.marker_ninja_purple);
                        iv_swastik.setVisibility(View.GONE);
                        tv_login.setText(email);
                        //rl_main.setBackgroundResource(R.mipmap.bg_map);
                    }
                    blink();
                    if(holdOne.equals("y")){
                        api_SaveAnswer();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (path.equals("payment")) {

                }
            }
            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext, message);
            }
            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //utility.showToast(mContext, getString(R.string.check_network));
            }
        });
        JsonObject object = Functions.getClient().getJsonMapObject(
                "email", email
        );
        baseRequest.callAPIPost(1, object, "index.php?method=getTransaction");
    }

    //Mansoonfly
    //flyfirst
    private void dialogPrimaryAccount(final String str) {
        final Dialog dialog=new Dialog(mContext);
        dialog.requestWindowFeature(1);
        dialog.setContentView(R.layout.dialog_primary_account);
        RecyclerView recyclerView= dialog.findViewById(R.id.rv_list);
        Ad_PrimaryAccounts ad_PrimaryAccounts=new Ad_PrimaryAccounts(SampleArrayList,mContext);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(ad_PrimaryAccounts);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
       // recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext,recyclerView, new AdapterView.OnItemClickListener));
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
                String email=sessionParam.email;
                if (str.equalsIgnoreCase("ninja")){
                    api_addEmail( email,"ninja");
                }else {
                    api_addEmail(email,"login");
                }
                str.equalsIgnoreCase("ninja");
                dialog.dismiss();

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        /*recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this.mContext, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            public void onLongItemClick(View view, int i) {
            }

            public void onItemClick(View view, int i) {
                Act_ImageMap act_ImageMap = Act_ImageMap.this;
                act_ImageMap.emailLogin = act_ImageMap.SampleArrayList.get(i);
                if (str.equalsIgnoreCase("ninja")) {
                    Act_ImageMap act_ImageMap2 = Act_ImageMap.this;
                    act_ImageMap2.api_addEmail(act_ImageMap2.emailLogin, "ninja");
                } else {
                    Act_ImageMap act_ImageMap3 = Act_ImageMap.this;
                    act_ImageMap3.api_addEmail(act_ImageMap3.emailLogin, "login");
                }
                str.equalsIgnoreCase("ninja");
                dialog.dismiss();
            }
        }));*/
        ((ImageView) dialog.findViewById(R.id.iv_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((TextView) dialog.findViewById(R.id.tv_proceed)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = -1;
        layoutParams.height = -2;
        window.setAttributes(layoutParams);
        dialog.show();
    }

    private void api_MakePayment(final String email, final String token) {
        baseRequest = new BaseRequest(mContext);
        baseRequest.hideLoader();
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                //utility.showToast(mContext,"test");
                sessionParam.paymentStatus(mContext, "y");
                iv_ninja_red.setImageResource(R.drawable.marker_ninja_red_paid);
                iv_ninja_blue.setImageResource(R.drawable.marker_ninja_blue_paid);
                iv_ninja_green.setImageResource(R.drawable.marker_ninja_green_paid);
                iv_ninja_purple.setImageResource(R.drawable.marker_ninja_purple_paid);
                sessionParam.saveEmail(mContext, email);
                tv_login.setText(email);
            }
            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext, message);
            }
            @Override
            public void onNetworkFailure(int requestCode, String message) {
               // utility.showToast(mContext, getString(R.string.check_network));
            }
        });
        JsonObject object = Functions.getClient().getJsonMapObject(
                "stripeToken", token,
                "email", email,
                "amount", "100",
                "currency", "usd",
                "description", "Membership Payment Android"
        );
        baseRequest.callAPIPost(1, object, "index.php?method=initiatePayment");
    }

    public void clickCheck(){
        v_ninja_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Toast.makeText(activity, "onclickwithoutclick", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void api_SaveAnswer() {
        baseRequest = new BaseRequest(mContext);
        baseRequest.hideLoader();
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {

            }
            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext, message);
            }
            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //utility.showToast(mContext, getString(R.string.check_network));
            }
        });
        JsonObject object = Functions.getClient().getJsonMapObject(
                "map_name", "Asakusa",
                "email",sessionParam.email,
                "red","1",
                "blue","0",
                "green","0",
                "purple","0"
        );
        baseRequest.callAPIPostwoLoader(1, object, "index.php?method=postAnswer");
    }


    //----------------------------Dialog Answered----------------------------------

    private void api_checkPayment(final String email) {
        baseRequest = new BaseRequest(mContext);
        baseRequest.hideLoader();
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                    if(object.toString().length()>2){
                        //transactiondone
                        iv_ninja_red.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_red_paid));
                        iv_ninja_blue.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_blue_paid));
                        iv_ninja_green.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_green_paid));
                        iv_ninja_purple.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_purple_paid));
                        sessionParam.paymentStatus(mContext, "y");
                    }else {

                        iv_ninja_red.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_red));
                        iv_ninja_blue.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_blue));
                        iv_ninja_green.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_green));
                        iv_ninja_purple.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_purple));
                        iv_swastik.setVisibility(View.GONE);
                        sessionParam.paymentStatus(mContext, "n");
                    }

            }
            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext, message);
            }
            @Override
            public void onNetworkFailure(int requestCode, String message) {
              //  utility.showToast(mContext, getString(R.string.check_network));
            }
        });
        JsonObject object = Functions.getClient().getJsonMapObject(

                "email", sessionParam.email

        );
        baseRequest.callAPIPost(1, object, "index.php?method=isTransactionDone");
    }






    private void api_sendPaymentStatus(String transId) {
        baseRequest = new BaseRequest(mContext);
        baseRequest.hideLoader();
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                   // if(object.toString().length()>2){
                        //transactiondone
                        iv_ninja_red.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_red_paid));
                        iv_ninja_blue.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_blue_paid));
                        iv_ninja_green.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_green_paid));
                        iv_ninja_purple.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_purple_paid));
                        sessionParam.paymentStatus(mContext, "y");
                   // }else {


                  //  }

            }
            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                iv_ninja_red.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_red));
                iv_ninja_blue.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_blue));
                iv_ninja_green.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_green));
                iv_ninja_purple.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.marker_ninja_purple));
                iv_swastik.setVisibility(View.GONE);
                sessionParam.paymentStatus(mContext, "n");
                //utility.showToast(mContext, message);
            }
            @Override
            public void onNetworkFailure(int requestCode, String message) {
               // utility.showToast(mContext, getString(R.string.check_network));
            }
        });
        JsonObject object = Functions.getClient().getJsonMapObject(

                "email", sessionParam.email,
                "transactionId",transId

        );
        baseRequest.callAPIPost(1, object, "index.php?method=doTransaction");
    }
    public void EnableRuntimePermission() {
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.GET_ACCOUNTS", "android.permission.READ_CONTACTS", "android.permission.READ_PHONE_STATE"}, 1);
    }

    /* access modifiers changed from: private */
    public boolean checkIfAlreadyhavePermissionForAccounts() {
        return ContextCompat.checkSelfPermission(this, "android.permission.GET_ACCOUNTS") == 0;
    }

    /* access modifiers changed from: private */
    public boolean checkIfAlreadyhavePermissionForContacts() {
        return ContextCompat.checkSelfPermission(this, "android.permission.READ_CONTACTS") == 0;
    }

    /* access modifiers changed from: private */
    public boolean checkIfAlreadyhavePermissionForStates() {
        return ContextCompat.checkSelfPermission(this, "android.permission.READ_PHONE_STATE") == 0;
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.GET_ACCOUNTS", "android.permission.RECEIVE_SMS", "android.permission.READ_SMS", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, 101);
    }



}
